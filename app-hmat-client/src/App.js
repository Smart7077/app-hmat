import Home from "./pages/Home/Home";
import Header from "./component/Header";
import Footer from "./component/Footer";
import {Switch,Route} from "react-router-dom";
import Contact from "./pages/Contact/Contact";
import Scient from "./pages/Scient/Scient";
import Study from "./pages/Study/Study";
import News from "./pages/News";
import SingleNews from "./pages/News/Single";
import Magistr from "./pages/Scient/Magistr";
import Doctarant from "./pages/Scient/Doctarant";
import Journal from "./pages/Scient/Journal";
import {Redirect} from "react-router";
import Shopping from "./pages/OnlineResource/Shopping";
import {ToastContainer} from "react-toastify";

function App() {
    return (
        <div>

            <Header/>
            <ToastContainer/>

            <Switch>
                <Route exact={true}  path="/" component={Home}></Route>
                <Route path="/contact" component={Contact}/>
                <Route path="/scient" component={Scient}/>
                <Route path="/study" component={Study}/>
                <Route path="/news" component={News}/>
                <Route path="/single" component={SingleNews}/>
                <Route path="/magistr" component={Magistr}/>
                <Route path="/doctarant" component={Doctarant}/>
                <Route path="/journal" component={Journal}/>
                <Route path="/shopping" component={Shopping}/>
                <Route path="*">
                    <Redirect to="/" />
                </Route>
            </Switch>

            <Footer/>
        </div>
    );
}

export default App;
