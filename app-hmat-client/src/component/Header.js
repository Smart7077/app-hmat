import React, {Component} from 'react';
import PropTypes from 'prop-types';

class Header extends Component {


    constructor(props) {
        super(props);
        this.state={
        }
    }
    render() {
        const currentMenu=localStorage.getItem('page')


        const changeMenu=(num)=>{
            localStorage.setItem('page',num)
        }
        console.log(currentMenu)
        return (
            <div className="home5 header5">
                <div id="wrapper">
                    <header id="header" className="site-header">
                        <div className="top-header">
                            <div className="container">
                                <div className="row">
                                    <div className="col-md-7 col-sm-7 col-xs-2">
                                    </div>

                                    <div className="col-md-5 col-sm-5 col-xs-10">
                                        <div className="top-right">
                                            <div className="language dropdown">
                                                <ul>
                                                    <li><a href="#">Uzbek</a>
                                                        <ul>
                                                            <li><a href="#">Uzbek</a></li>
                                                            <li><a href="#">English</a></li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>


                                            <div className="searchbox">
                                                <form method="get" className="search-form" action="#">
                                                    <label>
                                                        <span className="screen-reader-text">Search for:</span>
                                                        <input type="search" className="search-field"
                                                               placeholder="Qidirish" name="s"/>
                                                    </label>
                                                    <input type="submit" className="search-submit" value="Search"/>
                                                </form>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="mid-header">
                            <div className="container">
                                <div className="row">
                                    <div className="col-md-2 col-sm-4 col-xs-6">
                                        <div className="site-brand">
                                            <a className="logo" href="index5.html">
                                                <img src="assets/images/assets/logo70.jpg" alt="Universum"/>
                                            </a>
                                        </div>

                                    </div>

                                    <div className="col-md-10 col-sm-8 col-xs-6">
                                        <nav className="main-menu">
                                            <span className="mobile-btn"><i className="fa fa-bars"></i></span>
                                            <ul>
                                                <li className={currentMenu==1?"current-menu-item":''}><a onClick={()=>changeMenu(1)} href="/">Bosh sahifa</a>
                                                </li>
                                                <li className={`${currentMenu==2 && 'current-menu-item'}`}><a  href="#15">Faoliyat</a>
                                                    <ul className="sub-menu">
                                                        <li><a onClick={()=>changeMenu(2)} href="/study">O'quv faoliyati</a></li>
                                                        <li><a onClick={()=>changeMenu(2)} href="/scient">Ilmiy faoliyat</a></li>
                                                        <li><a onClick={()=>changeMenu(2)} href="#">Xalqaro faoliyat</a></li>
                                                        <li><a onClick={()=>changeMenu(2)} href="#">Ijtimoiy faoliyat</a></li>
                                                        <li><a onClick={()=>changeMenu(2)} href="#">Doktorantura</a></li>
                                                        <li><a onClick={()=>changeMenu(2)} href="#">Ishlab chiraqish va faoliyat</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li className={`${currentMenu==3 && 'current-menu-item'}`}><a href="" onClick={()=>changeMenu(3)}>Talabalarga</a>
                                                    <ul className="sub-menu">
                                                        <li><a onClick={()=>changeMenu(3)} href="event-grid.html">Faol talabalar</a></li>
                                                        <li><a onClick={()=>changeMenu(3)} href="/shopping">Online Resurslar</a></li>
                                                        <li><a onClick={()=>changeMenu(3)} href="single-event.html">BMI(Mavzu ,
                                                            annotatsiya,Rahbar)</a></li>
                                                        <li><a onClick={()=>changeMenu(3)} href="single-event.html">M.Dissertatsiya(mavzu ,
                                                            annotatsiya, Rahbar)</a>
                                                        </li>
                                                        <li><a onClick={()=>changeMenu(3)} href="single-event.html">Ishlab chiqarish amalyoti</a>
                                                        </li>
                                                        <li><a onClick={()=>changeMenu(3)} href="single-event.html">Reyting nazorati grafigi</a>
                                                        </li>
                                                        <li><a onClick={()=>changeMenu(3)} href="single-event.html">Olimpiada</a></li>
                                                    </ul>
                                                </li>
                                                <li className={`${currentMenu==4 && 'current-menu-item'}`}><a onClick={()=>changeMenu(4)} href="#1">Olimpiada</a>
                                                </li>
                                                <li className={`${currentMenu==5 && 'current-menu-item'}`}><a onClick={()=>changeMenu(5)} href="blog.html">Abiturent</a>
                                                </li>
                                                <li className={`${currentMenu==6 && 'current-menu-item'}`}><a onClick={()=>changeMenu(6)} href="404.html">Haqida</a></li>
                                                <li className={`${currentMenu==7 && 'current-menu-item'}`}><a onClick={()=>changeMenu(7)} href="/news">Yangiliklar</a></li>
                                                <li className={`${currentMenu==8 && 'current-menu-item'}`}><a onClick={()=>changeMenu(8)} href="/contact">Contact</a></li>
                                            </ul>
                                        </nav>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </header>
                </div>
            </div>
        );
    }
}

Header.propTypes = {};

export default Header;