import React, {Component} from 'react';
import PropTypes from 'prop-types';

class Footer extends Component {
    render() {
        return (
            <div className="home5 header5">
                <div id="wrapper">
                    <div id="bottom" className="site-bottom">
                        <div className="container">
                            <div className="footer-widget bottom1">
                                <div className="row">
                                    <div className="col-md-3 col-md-6">
                                        <div className="widget">
                                            <h3 className="widget-title">Kafedra haqida</h3>
                                            <div className="text-widget">
                                                <p>Mirzo Ulug'bek nomidagi O'zbekiston Respublikasi Milliy Universiteti</p>
                                            </div>
                                        </div>

                                    </div>

                                    <div className="col-md-3 col-md-6">
                                        <div className="widget">
                                            <h3 className="widget-title">Barcha kurslar</h3>
                                            <ul>
                                                <li><a href="#">Kurs nomi 1</a></li>
                                                <li><a href="#">Kurs nomi 2</a></li>
                                                <li><a href="#">Kurs nomi 3</a></li>
                                                <li><a href="#">Kurs nomi 4</a></li>
                                                <li><a href="#">Kurs nomi 5</a></li>
                                                <li><a href="#">Kurs nomi 6</a></li>
                                            </ul>
                                        </div>

                                    </div>

                                    <div className="col-md-3 col-md-6">
                                        <div className="widget">
                                            <h3 className="widget-title">Tezkor havolalar</h3>
                                            <ul>
                                                <li><a href="#">Kurslar</a></li>
                                                <li><a href="#">Online olimpiada </a></li>
                                                <li><a href="#">Olimpiada</a></li>
                                                <li><a href="#">Faol talabalar</a></li>
                                                <li><a href="#">Haqida</a></li>
                                                <li><a href="#">Aloqa</a></li>
                                            </ul>
                                        </div>

                                    </div>

                                    <div className="col-md-3 col-md-6">
                                        <div className="widget contact-widget">
                                            <h3 className="widget-title">Bog'lanish</h3>
                                            <ul>
                                                <li className="address">Тошкент шаҳри, Талабалар шаҳарчаси, Университет кўчаси 4- уй
                                                </li>
                                                <li className="mobile">(+998) 99 999 99 99<br/>(0 371) 246-02-24</li>
                                                <li className="email"><a
                                                    href="devonxona@nuu.uz">devonxona@nuu.uz</a>
                                                </li>
                                                <li className="time-work">Mon - Fri 8.00 - 18.00<br/>Sat 9.00 - 16.00
                                                </li>
                                            </ul>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                    <footer id="footer" className="site-footer">
                        <div className="container">
                            <div className="row">
                                <div className="col-md-6">
                                    <div className="copyright">
                                        <p>HMAT © 2020 Kafedra. <a href="http://hmat.nuu.uz/">nuu.uz</a></p>
                                    </div>

                                </div>

                                <div className="col-md-6">
                                    <nav className="nav-footer">
                                        <ul>
                                            <li><a href="#">Maxfiylik siyosati</a></li>
                                            <li><a href="#">Rad etish</a></li>
                                            <li><a href="#">Fikr-mulohaza</a></li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>

                    </footer>
                </div>
            </div>
        );
    }
}

Footer.propTypes = {};

export default Footer;