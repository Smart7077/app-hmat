import React, {Component} from 'react';
import {request} from "../utils/request";
import {toast,ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css'

class Shopping extends Component {
    componentDidMount() {
        request({
            url: '/booksType',
        }).then(res => {
            if (res.success) {
                this.setState({bookTypes: res.list})

            }
        });
        request({
            url: '/books'
        }).then(res => {
            if (res.success) {
                this.setState({books: res.list})

            }
        })
    }

    constructor(props) {
        super(props);
        this.state = {
            bookTypes: [],
            books: [],
        }
    }

    render() {

        const {books, bookTypes} = this.state


        const getBook = (link) => {
            request({
                url:'/file/documents/download/name/'+link
            }).then(res=>{
                if(res.success){
                    let a=document.createElement("a");
                    a.href='http://localhost:8080/file/documents/download/name/'+link;
                    document.body.appendChild(a);
                    a.target="_blank";
                    a.click();
                }
                else {
                    toast.error('Kitob topilmadi')
                }

            })


        }


        return (
            <div>
                <div id="pageloader">
                    <div className="pageloader">
                        <div className="thecube">
                            <div className="cube c1"/>
                            <div className="cube c2"/>
                            <div className="cube c4"/>
                            <div className="cube c3"/>
                        </div>

                        <div className="textedit">
                            <span className="site-name">Hmat</span>
                            <span className="site-tagline">Shopping</span>
                        </div>
                    </div>
                </div>
                <div id="wrapper">
                    <div id="content" className="site-content shop-content">
                        <div className="page-title parallax-window" data-parallax="scroll"
                             data-image-src="assets/images/placeholder/shop-title.jpg">
                            <div className="container">
                                <h1>Shop List</h1>
                                <div className="breadcrumb">
                                    <ul>
                                        <li><a href="/">Home</a></li>
                                        <li><span className="current">Shop</span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="container">
                            <div className="row">
                                <div id="sidebar" className="sidebar col-md-3">
                                    <aside className="widget">
                                        <h3 className="widget-title">Book Categories</h3>
                                        <ul>
                                            {bookTypes.length > 0 ?
                                                bookTypes.map((type, i) =>
                                                    <li key={i}><a href="" onClick={(e)=>(e.preventDefault())}>{type.name.uz}</a></li>
                                                ) : <h4>Book Types Not found</h4>
                                            }
                                        </ul>
                                    </aside>


                                </div>
                                <main id="main" className="site-main col-md-9">
                                    <div className="sort clearfix">
                                        <form action="#" className="ordering pull-left">
                                            <div className="selectbox">
                                                <select className="orderby" name="orderby">
                                                    <option value="menu_order">Default sorting</option>
                                                    <option value="popularity">popularity</option>
                                                    <option value="rating">average rating</option>
                                                    <option value="date">newness</option>
                                                    <option value="price">price: low to high</option>
                                                    <option value="price-desc">price: high to low</option>
                                                </select>
                                            </div>
                                        </form>

                                        <div className="style-switch pull-right clearfix">
                                            <a href="#" data-view="grid"><i className="fa fa-th"/></a>
                                            <a className="active" href="#" data-view="list"><i
                                                className="fa fa-th-list"/></a>
                                        </div>
                                    </div>

                                    <div className="products list layout column-4">
                                        {books.length > 0 ?
                                            books.map((item, i) =>
                                                    <div key={i} className="product">
                                                        <div className="p-inner shadow clearfix">
                                                            <span className="badge onnew">{i === 0 ? 'New' : ''}</span>
                                                            <div className="p-thumb">
                                                                <a href="#1"><img
                                                                    src={`/avatar/download/name/${item.img}`}
                                                                    alt=""/></a>
                                                            </div>

                                                            <div className="p-info">
                                                                <h3 className="p-title"><a href="">{item.name.uz}</a></h3>
                                                                <div className="p-meta">
                                                                    <span className="p-cat">by <a
                                                                        href="#">{item.authors.uz}</a></span>
                                                                </div>

                                                                <div className="p-desc">
                                                                    <p>In Furiously Happy, #1 New York Times bestselling
                                                                        author
                                                                        Jenny Lawson explores her lifelong battle with
                                                                        mental
                                                                        illness. A hysterical, ridiculous book about
                                                                        crippling
                                                                        depression and anxiety? That sounds like a terrible
                                                                        idea.
                                                                        Donec faucibus quam consectetur, elementum turpis
                                                                        ut,
                                                                        fringilla nisi. Mauris sit amet sollicitudin libero,
                                                                        maximus
                                                                        sollicitudin enim. </p>
                                                                </div>
                                                                <div className="p-rating">
                                                                    <div className="star-rating">
                                                                        <span style={{width: '80%'}}/>
                                                                    </div>
                                                                    <span className="review">
												<a href="#1">2 Review(s)</a> | <a href="#1">Add Your Review</a>
											</span>
                                                                </div>

                                                                <p className="price">
                                                                    <ins><span className="amount">Free</span></ins>
                                                                </p>
                                                            </div>

                                                            <div className="p-actions">
                                                                <a className="button rounded primary add-to-cart-button"
                                                                   onClick={() => getBook(item.link)} href="#1"><i
                                                                    className="fa fa-shopping-bag"></i> Download</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                            )
                                            : <h3>Books Not found</h3>}


                                    </div>

                                    <nav className="pagination">
                                        <ul>
                                            <li className="prev"><a href="#1">Previous</a></li>
                                            <li><span className="current">1</span></li>
                                            <li><a href="#1">2</a></li>
                                            <li><a href="#1">3</a></li>
                                            <li><a href="#1">4</a></li>
                                            <li><a href="#1">5</a></li>
                                            <li><span>...</span></li>
                                            <li><a href="#1">68</a></li>
                                            <li className="next"><a href="#1">Next</a></li>

                                        </ul>

                                    </nav>
                                </main>


                            </div>
                        </div>
                    </div>


                </div>
            </div>
        );
    }
}

Shopping.propTypes = {};

export default Shopping;