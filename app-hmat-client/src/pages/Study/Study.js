import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {request} from "../utils/request";
import Magistr from "../Scient/Magistr";
import Doctarant from "../Scient/Doctarant";
import Journal from "../Scient/Journal";
import StudyGraphic from "./StudyGraphic";
import StudyPlan from "./StudyPlan";
import LessonSchedule from "./LessonSchedule";

class Study extends Component {


    state={
        number:0
    }
    render() {

        const {number}=this.state


        function SwitchPage(n){
            switch (n){
                case 0:return <StudyGraphic/>
                case 1:return <StudyPlan/>
                case 2:return <LessonSchedule/>
            }
        }
        const getPage=(n)=>{
            this.setState({number:n})
        }


        return (
            <div>
                <div id="content" className="site-content">
                    <div className="page-title parallax-window" data-parallax="scroll"
                         data-image-src="assets/images/placeholder/event-title.jpg">
                        <div className="container">
                            <h1>Event Grid</h1>
                            <div className="breadcrumb">
                                <ul>
                                    <li><a href="/">Home</a></li>
                                    <li><span className="current">O'quv faoliyati</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="container">
                        <div className="row">
                            <main id="main" className="site-main col-md-12">
                                <div className="filter">
                                    <ul>
                                        <li><a className="active" onClick={()=>getPage(0)} href="#">O'quv grafigi</a></li>
                                        <li><a onClick={()=>getPage(1)} href="#" >O'quv rejalar</a></li>
                                        <li><a onClick={()=>getPage(2)} href="#" >Dars jadvali</a></li>
                                    </ul>
                                </div>
                                {SwitchPage(number)}

                            </main>
                        </div>
                    </div>
                    </div>

            </div>
        );
    }
}

Study.propTypes = {};

export default Study;