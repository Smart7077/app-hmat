export default function pegination(page,totalPages){
        let res = [];
        let from = 1;
        let to = totalPages;
        if (totalPages > 10) {
            from = Math.max(page - 2, 1);
            to = Math.max(Math.min(page + 2, totalPages), 5);
            if (to > 5) {
                res.push(1);
                if (from > 2) res.push(2);
                if (from > 3) {
                    if (from === 4) {
                        res.push(3);
                    } else {
                        res.push("...");
                    }
                }
            }
        }
        for (let i = from; i <= to; i++) {
            res.push(i);
        }

        if (totalPages > 10) {
            if (to < (totalPages - 2)) {
                if (to === 8) {
                    res.push(9);
                } else {
                    res.push("...");
                }
            }
            if (to < totalPages)
                res.push(totalPages - 1);
            if (to !== totalPages)
                res.push(totalPages);
        }
        return res;

}