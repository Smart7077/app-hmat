import React, {Component} from 'react';
import {request} from "../utils/request";
import {Link} from "react-router-dom";
import {Col, Pagination, PaginationItem, PaginationLink, Row} from "reactstrap";
import "./styless.css"
import ReactPaginate from 'react-paginate';
import pagination from "../utils/Pagination"


class News extends Component {
    componentDidMount() {
        request({
            url:'/category',
            Method:'GET'
        }).then(res=>{
            this.setState({categories:res.list})
        })

        request({
            url: '/news/pageable',
            method: 'GET',
            data: {
                page:this.state.page,
                size:this.state.size
            }
        }).then(res => {
                this.setState({
                    news: res.content,
                    totalPages:res.totalPages,
                    size:res.size,
                    page:res.pageable.pageNumber,
                    paginations:pagination(res.pageable.pageNumber,res.totalPages)
                });



            });


    }

    constructor() {
        super();
        this.state={
            categories:[],
            news:[],
            totalPages:0,
            page:0,
            size:6,
            paginations:[],
            currentPage:1
        }

    }


    render() {
        const {history}=this.props;
        const {page,size,totalPages,paginations,news,categories,currentPage}=this.state


        const getByCategory = (id) => {
            request({
                url:'/news/by/category/pageable',
                method:'GET',
                data:{
                    categoryId:id,
                    page:this.state.page,
                    size:this.state.size
                }
            }).then(res=>{
                this.setState({news:res.content})
            })
        }
        const getNews = (id) => {
            localStorage.setItem("singleNews",id);
        }
        const getNewsPegeable=(page)=> {
            this.setState({currentPage:page+1})
            console.log(page)
            request({
                url: '/news/pageable',
                method: 'GET',
                data: {
                    page:page,
                    size: size
                }
            }).then(res => {
                console.log(res);
                this.setState({
                    news: res.content,
                    totalPages: res.totalPages,
                    size:res.size,
                    page: res.number,
                    paginations: pagination(res.pageable.pageNumber, res.totalPages)
                });
            });
        }

        return (
            <div>

                <div id="pageloader">
                    <div className="pageloader">
                        <div className="thecube">
                            <div className="cube c1"></div>
                            <div className="cube c2"></div>
                            <div className="cube c4"></div>
                            <div className="cube c3"></div>
                        </div>

                        <div className="textedit">
                            <span className="site-name">Universum</span>
                            <span className="site-tagline">HTML Education Template</span>
                        </div>
                    </div>
                </div>


                <div id="wrapper">
                    <div id="content" className="site-content">
                        <div className="page-title parallax-window" data-parallax="scroll"
                             data-image-src="assets/images/placeholder/event-title.jpg">
                            <div className="container">
                                <h1>Event Grid</h1>
                                <div className="breadcrumb">
                                    <ul>
                                        <li><a href="/">Home</a></li>
                                        <li><span className="current">Event Grid</span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="container text-center">
                            <div className="row">
                                <main id="main" className="site-main col-md-12">
                                    <div className="filter">
                                        <ul>
                                            {categories.length > 0 ?
                                                categories.map((item, i) =>
                                                    i === 0 ?
                                                        <li><a href="" className="active"
                                                               onClick={() => getByCategory(item.id)}>{item.name.uz}</a>
                                                        </li>
                                                        : <li><a href=""
                                                                 onClick={() => getByCategory(item.id)}>{item.name.uz}</a>
                                                        </li>
                                                )
                                                : ''}

                                        </ul>
                                    </div>

                                    <div className="events layout column-3 isotope">
                                        {news.length > 0 ?
                                            news.map((item, i) =>
                                                <div key={i} className="event isotope-item paid">
                                                    <div className="event-inner shadow">
                                                        <div className="event-thumb">
                                                            <a className="mini-zoom" href="">
                                                                <img style={{width: 327, height: 198}}
                                                                     src={`/avatar/download/name/${item.avatar}`}
                                                                     alt=""/></a>
                                                        </div>

                                                        <div className="event-date">
                                                            <span>{new Date(item.createdDate).getDay()}</span>{new Date(item.createdDate).toDateString().slice(4, 7)}
                                                        </div>

                                                        <div className="event-info" style={{height: '23vh'}}>
                                                            <h3 className="post-title">
                                                                <Link to="/single" onClick={()=>getNews(item.id)} >{item.title.uz}</Link>
                                                            </h3>

                                                        </div>
                                                    </div>
                                                </div>
                                            )


                                            : <h1>Data Not Found</h1>}




                                    </div>


                                </main>
                            </div>
                            <nav className="pagination">
                                <ul>
                                    <li><a  href="#1"  onClick={page===0?(e)=>(e.preventDefault()):()=>getNewsPegeable(0)}>{"<<"}</a></li>
                                    <li><a  href="#1"  onClick={page===0?(e)=>(e.preventDefault()):()=>getNewsPegeable(page-1)}>{"<"}</a></li>
                                    {paginations.map(i=>
                                        <li key={i}   className={currentPage===(i)?"current":''}><a  href="#1"  onClick={page===(i-1)|| i === '...'?(e)=>(e.preventDefault()):()=>getNewsPegeable(i-1)}>{i}</a></li>
                                    )}
                                    <li><a href="#1" onClick={(totalPages-1)===page?(e)=>(e.preventDefault()):()=>getNewsPegeable(page+1)} >{'>'}</a></li>
                                    <li><a href="#1" onClick={(totalPages-1)===page?(e)=>(e.preventDefault()):()=>getNewsPegeable(totalPages-1)} >{'>>'}</a></li>
                                </ul>
                            </nav>

                        </div>

                    </div>

                </div>


            </div>

        );
    }
}

News.propTypes = {};
export default News;