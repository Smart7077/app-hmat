import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {request} from "../utils/request";

class SingleNews extends Component {
    componentDidMount() {

        const single=localStorage.getItem("singleNews");
        request({
            url:'/news',
            Method:'GET',
            data:{
                path:single
            }
        }).then(res=>{
            if(res.success){
                this.setState({currentNews:res})
            }
        })
    }

    state={
        currentNews:'',
    }




    render() {





        console.log(this.state.currentNews)
        return (
                <div>
                    {this.state.currentNews?
                        <div id="content" className="site-content">
                            <div className="single-course-title">
                                <div className="container">

                                    <div className="row">
                                        <div className="thumb">
                                            <img style={{width:400,height:300}} src={this.state.currentNews?`/avatar/download/name/2gRqRNXhKv.jpeg`:''} alt=""/>
                                        </div>

                                        <div className="info">
                                            <h1>{this.state.currentNews?this.state.currentNews.title.uz:''}</h1>

                                            <div className="course-detail">
                                                <ul>
                                                    <li>
                                                        {
                                                            this.state.currentNews?
                                                                new Date(this.state.currentNews.createdDate).toLocaleString('en-US', {'year': 'numeric','month':'long','day':'numeric',hour:'numeric',minute:'numeric'})

                                                                :''}</li>
                                                </ul>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <section id="course-about" className="course-section">
                                <div className="container">
                                    <h2 className="title">About the event</h2>
                                    <p>
                                        {this.state.currentNews.content.uz}
                                    </p>


                                </div>
                            </section>
                        </div>
                        :'Data Not Found'}
                </div>
        );
    }
}

SingleNews.propTypes = {};

export default SingleNews;