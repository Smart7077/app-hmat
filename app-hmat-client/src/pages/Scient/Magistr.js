import React, {Component} from 'react';
import PropTypes from 'prop-types';

class Magistr extends Component {
    render() {
        return (
            <div>
                <div className="page-content">
                    <div className="pull-right">
                        </div>
                    <p><b>Magistraturada pedagogik faoliyatga jalb etiladigan o‘qituvchilarga qo‘yiladigan umumiy malaka
                        talablari</b></p><p><i><u>Magistraturada pedagogik faoliyatga jalb etiladigan o‘qituvchi ta’lim
                    jarayoni doirasida:</u></i></p>
                    <ul>
                        <li>hamma turdagi o‘quv mashg‘ulotlarini yuqori darajada o‘tadi;</li>
                        <li>o‘quv, ilmiy va o‘quv-metodik ishlarni rejalashtiradi, tashkil etadi va nazorat qiladi;</li>
                        <li>ilmiy-tadqiqot ishlariga va magistratura talabalarining magistrlik dissertatsiyalarini
                            tayyorlash jarayoniga rahbarlik qiladi;
                        </li>
                        <li>kafedra ixtisosligi bo‘yicha ilmiy, ilmiy-tadqiqot seminarlarini tashkil etadi va ularda
                            ishtirok etadi;
                        </li>
                        <li>talabalar ilmiy seminarlarini boshqaradi;</li>
                        <li>kafedra pedagoglari tomonidan barcha turdagi o‘quv mashg‘ulotlari sifatli o‘tkazilishi
                            ustidan nazorat qiladi;
                        </li>
                        <li>magistraturaning tegishli mutaxassisligi bo‘yicha namunaviy va ishchi o‘quv rejalari va
                            dasturlarini ishlab chiqadi;
                        </li>
                        <li>kafedra ilmiy tadqiqotlari yo‘nalishlari doirasida tashkil etiladigan yig‘ilishlar va
                            konferentsiyalarda, shu jumladan xalqaro konferentsiyalarda ishtirok etadi;
                        </li>
                        <li>magistratura talabalarining mustaqil ishlarini tashkil etadi va rejalashtiradi;</li>
                        <li>magistratura talabalari uchun darsliklar va o‘quv-metodik qo‘llanmalarni ishlab chiqish va
                            nashrga tayyorlashda ishtirok etadi;
                        </li>
                        <li>magistratura talabasining kalendar ish rejasi bajarilishini nazorat qiladi.</li>
                    </ul>
                    <p><i><u>Magistraturada pedagogik faoliyatga jalb etiladigan o‘qituvchilarga quyidagi malaka
                        talablari qo‘yiladi:</u></i></p>
                    <ul>
                        <li>fanni yuqori darajada bilish hamda uning nazariy asoslarini va muhim yutuqlarga erishish
                            mumkin bo‘lgan yo‘llarni ko‘ra bilish uquviga ega bo‘lish;
                        </li>
                        <li>ilmiy muammolarni ishlab chiqa olish, tadqiqotlar o‘tkaza olish va ularning natijalari
                            bo‘yicha chop etilgan ilmiy ishlarga ega bo‘lish;
                        </li>
                        <li>magistratura talabalarining individual xususiyatlari, shaxsiy sifatlari va dissertatsiyalari
                            maqsadlarini hisobga olgan holda pedagogik faoliyat yo‘nalishlarini belgilash qobiliyatiga
                            ega bo‘lish;
                        </li>
                        <li>magistratura talabalarining bilim va malakalarni egallab olishga imkoniyat beradigan
                            innovatsion pedagogik texnologiyalarni qo‘llashga tayyor bo‘lish;
                        </li>
                        <li>pedagogik holatlar tahlili asosida pedagogik vazifalarni shakllantirish va ularni hal
                            etishning maqbul yechimlarini topish uquvi;
                        </li>
                        <li>magistratura talabalarining kasbiy qobiliyatlarini rivojlantirishga yo‘naltirilgan
                            innovatsion kurslar ishlab chiqish bo‘yicha amaliy tajribaga ega bo‘lish;
                        </li>
                        <li>ma’lumotlarni tizimlashtirish va izlash orqali o‘z bilimini takomillashtirish uchun doimiy
                            o‘rganish, ma’lumotlarni elektron qayta ishlashda axborot-kommunikatsiya va multimediya
                            texnologiyalaridan foydalanish mahoratiga ega bo‘lish.
                        </li>
                    </ul>
                    <p>Magistraturada pedagogik faoliyat yuritishga ilmiy unvon (ilmiy daraja)ga va kamida 3 yillik
                        uzluksiz ilmiy-pedagogik (ilmiy) ish stajiga ega bo‘lgan, shuningdek xorijiy mamlakatlarning
                        falsafa doktori (Ph.D) yoki unga tenglashtirilgan boshqa ilmiy darajali shaxslar jalb
                        qilinadi.</p><p><b>Mavzuni tanlash va ilmiy rahbarni tayinlash</b></p><p>Magistrlik
                    dissertatsiyasi o‘qish va o‘quv dasturlarini o‘zlashtirish jarayonida magistratura talabasi
                    egallagan nazariy va amaliy bilimlari asosida bajarilgan ilmiy-tadqiqot ishlarining natijasi
                    hisoblanadi.</p><p>Magistrlik dissertatsiyalarining mavzulari oliy ta’lim muassasalari kafedralarida
                    kafedralarning professor-o‘qituvchilari tarkibi, O‘zbekiston Respublikasi Fanlar akademiyasi ilmiy
                    muassasalari, tarmoq ilmiy-tadqiqot va loyihalash muassasalari xodimlari tomonidan
                    shakllantiriladi.</p><p>Magistrlik dissertatsiyalarining mavzulari, qoidaga ko‘ra, dolzarb
                    ilmiy-tadqiqot masalalariga yoki aniq amaliy vazifalarni hal etishga bag‘ishlanadi.</p><p>Magistrlik
                    dissertatsiyasiga ilmiy rahbarlik oliy ta’lim muassasalarida ishlaydigan professorlar, fan
                    doktorlari, dotsentlar, fan nomzodlari, O‘zbekiston Respublikasi Fanlar akademiyasi ilmiy
                    muassasalari olimlari, tashkilotlarning yuqori malakali va tajribali mutaxassislari, shuningdek
                    belgilangan tartibda magistrlik, ilmiy darajalar yoki ilmiy unvonlarga ega bo‘lgan xorijiy
                    mutaxassislar tomonidan amalga oshiriladi.</p><p>Magistratura talabasi o‘qiyotgan oliy ta’lim
                    muassasasida ishlamaydigan xodimlardan unga ilmiy rahbar tayinlangan taqdirda, talabaga qo‘shimcha
                    ravishda tegishli kafedra professor-o‘qituvchilari tarkibidan ilmiy maslahatchi tayinlanadi.</p>
                    <p>Belgilangan talablarga muvofiq magistrlik dissertatsiyasini bajarish doirasida ilmiy rahbar bilan
                        bir xil mas’uliyat ilmiy maslahatchiga ham yuklatiladi.</p><p>Magistratura talabasi o‘qiyotgan
                    oliy ta’lim muassasasi xodimlari tarkibidan bo‘lgan ilmiy rahbar va ilmiy maslahatchining magistrlik
                    dissertatsiyasini tayyorlash bo‘yicha talabalar bilan ishlashi ularning oliy ta’lim muassasasida
                    o‘quv yili shaxsiy ish rejasi bilan belgilanadigan o‘quv yuklamasiga kiritiladi.</p><p>Magistratura
                    talabasi o‘qiyotgan oliy ta’lim muassasasida ishlamaydigan xodimlardan bo‘lgan ilmiy rahbar
                    mehnatiga haq to‘lash belgilangan tartibda mehnatga soatbay haq to‘lash shartlari bo‘yicha bir o‘quv
                    yiliga bir magistratura talabasi uchun 50 soatdan oshmagan hajmda, tegishli oliy ta’lim muassasasi
                    xarajatlar smetasida bu maqsadlar uchun nazarda tutilgan mablag‘lar doirasida amalga oshiriladi.</p>
                    <p>Professor yoki fan doktori beshtagacha, shuningdek dotsent, fan nomzodi va mutaxassis uchtacha
                        magistrlik dissertatsiyasiga rahbarlikni amalga oshirishi mumkin.</p><p>Kafedralar tomonidan
                    taqdim etilgan va oliy ta’lim muassasasi yoki fakultet o‘quv-metodik kengashida muhokama qilingan
                    magistratura talabalarining magistrlik dissertatsiyalari mavzulari va ularning ilmiy rahbarlari
                    (ilmiy maslahatchilari) ilmiy ishlar bo‘yicha prorektor (direktor o‘rinbosari) taqdimnomasiga asosan
                    magistratura talabalari o‘qiyotgan birinchi o‘quv yilining birinchi ikki oyi ichida tegishli oliy
                    ta’lim muassasasi rektori (direktori)ning buyrug‘i bilan tasdiqlanadi.</p><p><i><u>Ilmiy rahbarning
                    majburiyatlariga quyidagilar kiradi:</u></i></p><p></p>
                    <ul>
                        <li><span>tadqiqot mavzusi doirasida yuzaga kelishi mumkin bo‘lgan masalalar bo‘yicha tizimli ravishda yordam berish maqsadida maslahatlar jadvalini tuzish;</span><br/>
                        </li>
                        <li><span>tadqiqot usullarini tanlashda ishtirok etish va ularni tadqiqot ishida qo‘llashda magistratura talabasiga ko‘maklashish;</span><br/>
                        </li>
                        <li><span>magistratura talabasining belgilangan kalendar ish rejasi bo‘yicha ishlarning bajarilishini va magistrlik dissertatsiyasi o‘z vaqtida tayyorlanishini nazorat qilish;</span><br/>
                        </li>
                        <li><span>dastlabki himoyagacha magistrlik dissertatsiyasiga xulosa berish.</span><br/>
                        </li>
                    </ul>
                    <p></p><p className=""><b>Magistrlik dissertatsiyasining tuzilishi va uning mazmuniga qo‘yiladigan
                    talablar</b></p><p><i><u>Magistrlik dissertatsiyasi quyidagi tarkibiy qismlardan iborat bo‘lishi
                    kerak:</u></i></p>
                    <ul>
                        <li>titul varaq;</li>
                        <li>ikki tilda (o‘qitish tili va ingliz tilida) magistrlik dissertatsiyasining qisqacha
                            annotatsiyasi;
                        </li>
                        <li>mundarija;</li>
                        <li>kirish;</li>
                        <li>asosiy qism;</li>
                        <li>xulosa;</li>
                        <li>adabiyotlar ro‘yxati;</li>
                        <li>ilovalar (mavjud bo‘lsa).</li>
                    </ul>
                    <p><i><u>Kirish quyidagilarni qisqacha o‘z ichiga olishi lozim:</u></i></p>
                    <ul>
                        <li>magistrlik dissertatsiyasi mavzusining asoslanishi va uning dolzarbligi;</li>
                        <li>tadqiqot ob’ekti va predmeti;</li>
                        <li>tadqiqot maqsadi va vazifalari;</li>
                        <li>ilmiy yangiligi;</li>
                        <li>tadqiqotning asosiy masalalari va farazlari;</li>
                        <li>tadqiqot mavzusi bo‘yicha adabiyotlar sharhi (tahlili);</li>
                        <li>tadqiqotda qo‘llanilgan metodikaning tavsifi;</li>
                        <li>tadqiqot natijalarining nazariy va amaliy ahamiyati;</li>
                        <li>ish tuzilmasining tavsifi.</li>
                    </ul>
                    <p><i><u>Magistrlik dissertatsiyasining asosiy qismi kamida uch bobdan iborat bo‘lib, boblar hajm
                        jihatidan o‘zaro mutanosib bo‘lishi va quyidagilarni o‘z ichiga olishi lozim:</u></i></p>
                    <ul>
                        <li>tadqiqot mavzusiga taalluqli boshqa manbalarda keltirilgan nazariy, amaliy va empirik
                            tadqiqotlar natijalarining tanqidiy tahlili;
                        </li>
                        <li>tadqiqot metodikasi va ishning amaliy qismi bayoni;</li>
                        <li>tadqiqot olib borilgan masalani hal etishda magistratura talabasining shaxsiy hissasi
                            ko‘rsatilgan holda tadqiqotning asosiy natijalari bayoni.
                        </li>
                    </ul>
                    <p>Magistrlik dissertatsiyasining xulosa qismida barcha boblarda qayd etilgan natijalarning ilmiy va
                        amaliy ahamiyati, shuningdek ilmiy tadqiqot muammosini hal etish bo‘yicha xulosalar yoritiladi.
                        Xulosa qismi 4 sahifadan oshmasligi kerak.</p><p>Magistrlik dissertatsiyasiga uning mazmunini
                    bayon etish uchun bevosita zarur bo‘lgan qo‘shimcha ma’lumotlarni o‘z ichiga olgan materiallar ilova
                    qilinishi mumkin. Ilova qismining hajmi magistrlik dissertatsiyasi umumiy hajmining uchdan bir
                    qismidan oshmasligi lozim.</p><p>Magistrlik dissertatsiyasi ustida ishlayotgan magistratura talabasi
                    kasb odob-axloqi qoidalariga rioya etishi (plagiat, ma’lumotlarni soxtalashtirish, shuningdek
                    yolg‘on tsitatalar keltirishga yo‘l qo‘ymaslik) lozim.</p><p><i><u>Dissertatsiya matni standart
                    varaqda yozilgan bo‘lib, unda quyidagi qoidalarga rioya etilgan bo‘lishi lozim:</u></i></p>
                    <ul>
                        <li>qatorlar oralig‘i — 1,5 sm;</li>
                        <li>yuqori va pastki hoshiya 2 sm, satr boshi: chap tomondan 3 sm, o‘ng tomondan 2 sm;</li>
                        <li>xatboshilar orasidagi oraliq — 5 yoki 6 belgili.</li>
                        <li>Magistrlik dissertatsiyasi matnini Misrosoft Word matnli redaktorida Times New Roman
                            shriftida yozish tavsiya etiladi.
                        </li>
                    </ul>
                    <p>Magistrlik dissertatsiyasining hajmi titul varaq, mundarija, adabiyotlar ro‘yxati va ilovalardan
                        tashqari 70 — 80 sahifa bo‘lishi tavsiya etiladi.</p><p>Magistrlik dissertatsiyasi magistratura
                    talabasi o‘qigan tilda (kafedra yoki magistratura bo‘limi tavsiyasiga muvofiq chet tilda)
                    tayyorlanadi. Chet tilda tayyorlangan magistrlik dissertatsiyasiga davlat tilida yozilgan
                    annotatsiya ilova etiladi. Bunday magistrlik dissertatsiyalarining dastlabki va rasmiy himoyalari
                    tarjima bilan o‘tkaziladi.</p><p>Magistratura mutaxassisligi xususiyati hisobga olingan holda
                    magistrlik dissertatsiyasi tarkibiy qismlarining mazmuni va hajmi fakultet O‘quv-metodik
                    kengashining qarori bilan o‘zgartirilishi va ko‘paytirilishi mumkin.</p><p><b>Dastlabki va rasmiy
                    himoyani o‘tkazish tartibi</b></p><p>Magistrlik dissertatsiyasining dastlabki himoyasi ilmiy rahbar
                    (ilmiy maslahatchi) ishtirokida kafedra tomonidan tuzilgan komissiyada tashkil etiladi.</p>
                    <p>Dastlabki himoyaga boshqa kafedralardan, shuningdek boshqa tashkilotlardan mutaxassislar taklif
                        etilishi mumkin.</p><p>Ilmiy maslahatchi tayinlangan holda dastlabki himoyaga qadar undan
                    magistrlik dissertatsiyasiga xulosa olish ham talab etiladi.</p><p>Dastlabki himoyaga qadar
                    magistratura talabasi ichki va tashqi hamda ilmiy rahbar taqrizlariga, shuningdek dissertatsiya
                    mavzusiga doir kamida 2 ta ilmiy maqola yoki tezisga ega bo‘lishi kerak.</p><p>Dastlabki himoya
                    yakunlari kafedralar yig‘ilishi bayonnomasi bilan rasmiylashtiriladi.</p><p>Ichki va tashqi
                    taqrizchilarni oliy ta’lim muassasasining tegishli kafedrasi tavsiya etadi va ularning ro‘yxati
                    ilmiy ishlar bo‘yicha prorektor (direktor o‘rinbosari) tomonidan tasdiqlanadi.</p><p><i><u>Taqrizchi
                    vazifalariga quyidagilar kiradi:</u></i></p>
                    <ul>
                        <li>magistrlik dissertatsiyasining dolzarbligi, ilmiy yangiligi va tugallanganligi to‘g‘risida
                            xulosa taqdim etish;
                        </li>
                        <li>dastlabki himoyadan kamida 3 kun oldin taqriz taqdim etish;</li>
                        <li>kasb odob-axloqi qoidalarining buzilishi holatlari (plagiat, ma’lumotlarni soxtalashtirish,
                            yolg‘on tsitata keltirish va boshqalar) aniqlangan taqdirda, ularni taqrizda ko‘rsatish.
                        </li>
                    </ul>
                    <p>Magistrlik dissertatsiyasi belgilangan talablarga mos kelmagan holatlarda, magistratura talabasi
                        tomonidan kasb odob-axloqi qoidalari (plagiat, ma’lumotlarni soxtalashtirish, yolg‘on tsitata
                        keltirish va boshqalar) buzilganligi aniqlangan taqdirda, shuningdek ushbu holatlarni qisqa
                        muddatda tuzatish imkoniyati mavjud bo‘lmaganda taqrizchi magistrlik dissertatsiyasini himoyaga
                        qo‘yish maqsadga muvofiq emasligi to‘g‘risida xulosa beradi.</p><p>Magistrlik dissertatsiyasini
                    rasmiy himoya qilish kuni oliy ta’lim muassasasi rektori (direktori)ning buyrug‘i bilan tasdiqlangan
                    jadval asosida belgilanadi.</p><p>Magistrlik dissertatsiyasining rasmiy himoyasi oliy ta’lim
                    muassasasining Davlat yakuniy attestatsiya komissiyasi (keyingi o‘rinlarda Komissiya deb ataladi)
                    tomonidan o‘tkaziladi.</p><p>Magistratura talabasi magistrlik dissertatsiyasining rasmiy himoyasi
                    taqdimot materiallari bilan bayon etilishi va 20 daqiqadan oshmasligi lozim.</p><p><i><u>Rasmiy
                    himoyada Komissiya a’zolari magistratura talabasini quyidagi mezonlar asosida baholaydi:</u></i></p>
                    <ul>
                        <li>magistrlik dissertatsiyasi tadqiqot mavzusining dolzarbligini va uning amaliyot bilan
                            bog‘liqligini ko‘rsatib bera olishi;
                        </li>
                        <li>magistratura talabasining tadqiqotga va vazifalarni hal etishga mustaqil yondashuvi;</li>
                        <li>foydalanilgan ilmiy adabiyotlar, ilmiy nashrlar, normativ-huquqiy hujjatlar, statistik
                            ma’lumotlar, shuningdek xorijiy tillardagi adabiyotlar tanqidiy tahlilining to‘liqligi va
                            chuqurligi;
                        </li>
                        <li>tadqiqot usullari amaliyotda qo‘llanilganligining asoslanganligi;</li>
                        <li>olingan natijalar asosida ishlab chiqilgan tavsiyalarning amaliy ahamiyati;</li>
                        <li>magistratura talabasining magistrlik dissertatsiyasi doirasida o‘tkazilgan tadqiqotlar va
                            olingan natijalarni rivojlantirish istiqbollarini ko‘ra bilish qobiliyati;
                        </li>
                        <li>magistrlik dissertatsiyasining nazariy va amaliy qismlaridagi o‘zaro mantiqiy bog‘liqlikni
                            kuzata bilish malakasi.
                        </li>
                    </ul>
                    <p>«Qoniqarsiz» baho qo‘yilganda yoki magistrlik dissertatsiyasi rasmiy himoyaga qo‘yilmagan
                        taqdirda magistratura talabasi keyingi 3 yil davomida uni qayta himoya qilish huquqiga ega.</p>
                    <p>Magistrlik dissertatsiyalari rasmiy himoyasi natijalari oliy ta’lim muassasasi Ilmiy kengashida
                        muhokama etiladi.</p><p>Himoya qilingan magistrlik dissertatsiyalari oliy ta’lim muassasasida 3
                    yil davomida saqlanadi.</p>
                        </div>
            </div>
        );
    }
}

Magistr.propTypes = {};

export default Magistr;