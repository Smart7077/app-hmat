import React, {Component} from 'react';
import PropTypes from 'prop-types';

class Doctarant extends Component {
    render() {
        return (
            <div>
                <div className="page-content">
                    <div className="pull-right">
                        <a className="print" href="/page/print/5df6535ef2c2dc1d66097a94" target="_blank"><i
                            className="icon icon-newspaper"></i> Chop etish</a></div>
                    <p><span>Dissertatsiyani tayyorlash va rasmiylashtirish</span></p>
                    <ul>
                        <li><span>Dissertatsiya ishiga umumiy talablar</span></li>
                        <li><font color="#003580" face="Verdana, Geneva, sans-serif"><span><a
                            href="http://new.tuit.uz/ilmiy-doklad-korinishidagi-dissertatsiya" target="_blank">Ilmiy doklad sifatidagi dissertatsiya</a></span></font>
                        </li>
                        <li><font color="#003580" face="Verdana, Geneva, sans-serif"><span><a
                            href="http://new.tuit.uz/dissertatsiya-strukturasi" target="_blank">Dissertatsiya strukturasi</a></span></font>
                        </li>
                        <li><font color="#003580" face="Verdana, Geneva, sans-serif"><span><a
                            href="http://new.tuit.uz/dissertatsiya-matnini-bolimlarga-ajratish" target="_blank">Dissertatsiya matnini bo‘limlarga ajratish</a></span></font>
                        </li>
                        <li><span>Dissertatsiya matnini shakllantirish</span></li>
                        <li className=""><span><font color="#003580" face="verdana, sans-serif"></font><font
                            color="#003580" face="verdana, sans-serif"></font><a
                            href="http://new.tuit.uz/jadvallar-formulalar-va-rasmlarni-shakllantirish" target="_BLANK">Jadvallar, formula va rasmlarni shakllantirish</a></span><br/>
                        </li>
                        <li><span>Dissertatsiya qo‘lyozmasini tayyorlashga talablar</span></li>
                        <li><font color="#003580" face="Verdana, Geneva, sans-serif"><span><a
                            href="http://new.tuit.uz/foydalanilgan-adabiyotlar-royxatini-tayyorlash" target="_blank">Foydalanilgan adabiyotlar ro‘yxatini tayyorlash</a></span></font>
                        </li>
                        <li><font color="#003580" face="Verdana, Geneva, sans-serif"><span><a
                            href="http://new.tuit.uz/dissertatsiyani-himoyasi-uchun-plakatlarni-tayyorlash"
                            target="_blank">Dissertatsiyani himoya qilish uchun ko‘rgazmali materiallarni tayyorlash</a></span></font>
                        </li>
                        <li><font color="#003580" face="Verdana, Geneva, sans-serif"><span><a
                            href="http://new.tuit.uz/dissertasiyani-rasmiylashtirish-qoidalari" target="_blank">Dissertatsiyani rasmiylashtirish qoidalari</a></span></font>
                        </li>
                        <li><font color="#003580" face="Verdana, Geneva, sans-serif"><span><a
                            href="http://new.tuit.uz/doktorantura#" target="_blank">Ko‘rgazmali misollar (Dissertatsiya titul varag‘i, dissertatsiya mundarijasi, dissertatsiyaning kirish qismini tayorlash varianti, dissertatsiyaning xulosa qismini tayorlash varianti, foydalanilgan adabiyotlar ro‘yxatini tayyorlash misollari, jadvallarni shakllantirish misollari, rasmlar va grafik ob’ektlarni shakllantirish misollari, avtoreferat muqovasi)</a></span></font>
                        </li>
                    </ul>
                    <p><span>Dissertatsiyani himoya qilish</span></p>
                    <ul>
                        <li><font color="#003580" face="Verdana, Geneva, sans-serif"><span><a
                            href="http://new.tuit.uz/doktorantura#" target="_blank">Kengashga ariza</a></span></font>
                        </li>
                        <li><font color="#003580" face="Verdana, Geneva, sans-serif"><span><a
                            href="http://new.tuit.uz/dissertatsiya-ishining-dastlabki-ekspertizasi" target="_blank">Dissertatsiya ishining dastlabki ekspertizasi</a></span></font>
                        </li>
                        <li><font color="#003580" face="Verdana, Geneva, sans-serif"><span><a
                            href="http://new.tuit.uz/dissertatsiya-ishini-ixtisoslashgan-kengashga-qabul-qilish"
                            target="_blank">Dissertatsiyani ixtisoslashgan kengashga qabul qilish</a></span></font></li>
                        <li><font color="#003580" face="Verdana, Geneva, sans-serif"><span><a
                            href="http://new.tuit.uz/yetakchi-tashkilot-taqrizi" target="_blank">Yetakchi tashkilot taqrizi</a></span></font>
                        </li>
                        <li><font color="#003580" face="Verdana, Geneva, sans-serif"><span><a
                            href="http://new.tuit.uz/doktorantura#" target="_blank">Yetakchi tashkilot taqrizini tayyorlash varianti</a></span></font>
                        </li>
                        <li><span>Rasmiy opponent taqrizi</span></li>
                        <li><font color="#003580" face="Verdana, Geneva, sans-serif"><span><a
                            href="http://new.tuit.uz/doktorantura#" target="_blank">Rasmiy opponent taqrizini tayyorlash varianti</a></span></font>
                        </li>
                        <li><font color="#003580" face="Verdana, Geneva, sans-serif"><span><a
                            href="http://new.tuit.uz/ilmiy-rahbar-taqrizi"
                            target="_blank">Ilmiy rahbar taqrizi</a></span></font></li>
                        <li><font color="#003580" face="Verdana, Geneva, sans-serif"><span><a
                            href="http://new.tuit.uz/avtoreferatga-taqrizlar"
                            target="_blank">Avtoreferatga taqrizlar</a></span></font></li>
                        <li><span>Dissertatsiya himoyasi tartibiga tayyorgarlik</span></li>
                        <li><font color="#003580" face="Verdana, Geneva, sans-serif"><span><a
                            href="http://new.tuit.uz/dissertatsiya-ishi-natijalari-maruzasi-haqida" target="_blank">Dissertatsiya ishi natijalari ma’ruzasi haqida</a></span></font>
                        </li>
                        <li><font color="#003580" face="Verdana, Geneva, sans-serif"><span><a
                            href="http://new.tuit.uz/dissertatsiya-himoyasi-tartibi" target="_blank">Dissertatsiya himoyasi tartibi</a></span></font>
                        </li>
                        <li><span><font color="#003580" face="verdana, sans-serif"><span><a
                            href="http://new.tuit.uz/doktorantura#" target="_blank">Ixtisoslashgan kengash ekspert xay'ati xulosasi</a></span></font>&nbsp;(<font
                            color="#003580" face="verdana, sans-serif"></font><a
                            href="http://new.tuit.uz/doktorantura#"><font color="#003580"
                                                                          face="verdana, sans-serif"><span>M</span></font>isol</a>)</span><br/>
                        </li>
                        <li><font color="#003580" face="Verdana, Geneva, sans-serif"><span><a
                            href="http://new.tuit.uz/doktorantura#" target="_blank">Ixtisoslashgan kengash xulosasi loyihasi</a></span></font>
                        </li>
                    </ul>
                    <p><span>Tashkiliy ishlar</span></p>
                    <ul>
                        <li><font color="#003580" face="Verdana, Geneva, sans-serif"><span><a
                            href="http://new.tuit.uz/dissertatsiya-mavzusini-elon-qilish" target="_blank">Dissertatsiya mavzusini e’lon qilish</a></span></font>
                        </li>
                        <li><span><font color="#003580" face="verdana, sans-serif"><span><a
                            href="http://new.tuit.uz/dissertatsiya-himoyasini-elon-qilish" target="_blank">Dissertatsiya himoyasini e’lon qilish</a></span></font>&nbsp;
                            <span>(<a
                                href="http://new.tuit.uz/doktorantura#" target="_blank">e'lon matniga misol</a>)</span></span>
                        </li>
                        <li><font color="#003580" face="Verdana, Geneva, sans-serif"><span><a
                            href="http://new.tuit.uz/doktorantura#" target="_blank">Dissertatsiya ishi natijalarini tadbiq qilish dalolatnomasini tuzish</a></span></font>
                        </li>
                    </ul>
                </div>
            </div>
        );
    }
}

Doctarant.propTypes = {};

export default Doctarant;