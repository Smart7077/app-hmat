import React, {Component} from 'react';
import PropTypes from 'prop-types';

class Journal extends Component {
    render() {
        return (
            <div>
                <div className="page-content">
                    <div className="pull-right">
                        <a className="print" href="/page/print/5df6535ef2c2dc1d66097a95" target="_blank"><i
                            className="icon icon-newspaper"></i> Chop etish</a></div>
                    <div className="panel panel-default">
                        <div className="panel-heading"><span>Bo'limlar:</span></div>
                        <ul className="list-group">
                            <li className="list-group-item"><span>2015-yil</span></li>
                            <li className="list-group-item"><span>1-son <br/></span></li>
                            <li className="list-group-item"><span>2-son</span></li>
                            <li className="list-group-item"><a
                                href="https://tuit.uz/static/uploads/files/p/i/587f00d8c8fc2.pdf"><span>3-son</span></a>
                            </li>
                            <li className="list-group-item"><a
                                href="https://tuit.uz/static/uploads/files/u/a/587c5a8a76818.pdf"><span>4-son</span></a>
                            </li>
                            <li className="list-group-item"><span>2016-yil</span></li>
                            <li className="list-group-item"><span>1-son <br/></span></li>
                            <li className="list-group-item"><a
                                href="https://tuit.uz/static/uploads/files/w/i/587c5a4cbc7ca.pdf"><span>2-son</span></a>
                            </li>
                            <li className="list-group-item"><span>3-son</span></li>
                            <li className="list-group-item"><span>4-son</span></li>
                            <li className="list-group-item"><span>2017-yil</span></li>
                            <li className="list-group-item"><span>2018-yil</span></li>
                            <li className="list-group-item"><a
                                href="https://tuit.uz/static/uploads/files/e/l/5c5170d9892d0.rar"
                                target="_blank">1-son</a> <br/></li>
                            <li className="list-group-item"><a
                                href="https://tuit.uz/static/uploads/files/v/o/5c517157ef100.rar"
                                target="_blank">2-son</a><br/></li>
                            <li className="list-group-item"><span>3-son</span></li>
                            <li className="list-group-item"><span>4-son</span></li>
                            <li className="list-group-item"><span>2019-yil</span></li>
                        </ul>
                    </div>
                    <p>
                        <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Maqolaga
                            qo‘yiladigan talablar</b><br/><i><b>Umumiy talablar</b></i></p><p align="justify">1. Jurnalga
                    maqolalar o‘zbek, ingliz va rus tillarida qabul qilinadi. Shuningdek, maqola to‘liq ingliz tiliga
                    tarjima qilingan bo‘lishi lozim.</p><p align="justify">2. Maqola nomi, mazmunnoma (annotatsiya),
                    kalit so‘zlar o‘zbek, ingliz va rus tillarida berilishi lozim.</p><p align="justify">3. UDK
                    keltirilishi lozim.</p><p align="justify">4. Maqola muallif(lar)i haqida ma’lumot berilishi kerak. U
                    o‘z ichiga quyidagilarni oladi (alohida varaqda):</p>
                    <ul>
                        <li>Muallif (lar) familiyasi ismi sharifi (to‘liq),</li>
                        <li>Ish joyi va lavozimi qisqartirishlarsiz,</li>
                        <li>Telefoni va elektron pochtasi,</li>
                        <li>Maqola qo‘yiladigan jurnal rubrikasi nomi.</li>
                    </ul>
                    <p align="justify">5. Maqolaga taqriz va ochiq chop etish mumkinligini ko‘rsatuvchi ekspert xulosasi
                        taqdim etilishi lozim.</p><p align="justify">6. Tahririyatga maqolani chop etilgan holdagi bir
                    nusxasi kerakli hujjatlari bilan birga va elektron varianti taqdim etiladi.</p><p align="justify">7.
                    Maqola aniq masala qo‘yilishidan, tadqiqot usulidan iborat bo‘lishi, olingan natijalarning mazmuni
                    va ularni qo‘llanish sohalarini ko‘rsatish kerak.</p><p align="justify">8. Topshirilayotgan
                    maqolalar rasmlar va adabiyotlar bilan birga 15 varaqdan oshmasligi kerak.</p>
                    <ul></ul>
                    <p className=""><b><i>Umumiy qoidalar</i></b></p>
                    <div align="justify">
                        <ol>
                            <li>Maqolada keltirilgan barcha ma’lumotlarga muallif(lar) javobgar.</li>
                            <li>Tahririyat maqolani qaytarmaydi va izohlamaydi.</li>
                            <li>Tahririyat maqolani qo‘shimcha ilmiy ekspertizaga jo‘natish huquqiga ega.</li>
                            <li>Agarda maqola ilmiy qimmatga ega bo‘lmasa, talabga ko‘ra rasmiylashtirilmagan bo‘lsa
                                yoki boshqa shaklda rasmiylashtirilgan bo‘lsa maqolani qabul qilmaslik huquqiga ega.
                            </li>
                        </ol>
                    </div>
                    <p><b><i>Jurnal rubrikalari</i></b></p>
                    <ul>
                        <li>Informatika va axborot texnologiyalari.</li>
                        <li>Infokommunikatsion tarmoqlar va tizimlar</li>
                        <li>Radiotexnika, radioaloqa va teleradioeshittirish</li>
                        <li>Matematik modellashtirish va dasturlash</li>
                        <li>Axborot xavfsizligi</li>
                        <li>Mikroelektronika va sxemotexnika.</li>
                        <li>Ta’limni axborotlashtirish</li>
                        <li>Ijtimoiy-iqtisodiy va gumanitar muammolar.</li>
                        <li>Ilmiy ma’lumot.</li>
                    </ul>
                    <p><b><i>Rasmiylashtirish qoidalari</i></b></p><p>1. Maqola Microsoft Word matnli muharririning
                    97-2003 versiyasida yozilgan bo’lishi lozim (yuqori versiyalar qabul qilinmaydi).</p><p>2. Hujjat
                    maydoni: tepadan – 2 sm, pastdan – 2 sm, chapdan – 3 sm, o’ngdan – 1.5 sm. Raqamlash past va
                    o’rtadan.</p><p>3. Birlik interval, abzats – 1 sm.</p><p>4. Barcha maqola faqat Times New Roman – 14
                    shriftida terilgan bo’lishi lozim.</p><p>5. Formulalar:</p><p></p>
                    <ul>
                        <li>
                            <span>formulalar Microsoft Word 97-2003 ning formulalar redaktorida yozilishi lozim;</span><br/>
                        </li>
                        <li><span>markazlashtiriladi;</span><br/></li>
                        <li><span>formulalar ketma-ket uzluksiz raqamlanadi, raqam hujjatning o’ng tarafidan aylanali qavslarda beriladi (masalan, (4));</span><br/>
                        </li>
                        <li><span>rasm ko’rinishida saqlangan formulalardan foydalanishga ruxsat berilmaydi;</span><br/>
                        </li>
                        <li><span>formuladan oldin va keyin bo’sh satr bo’lishi kerak.</span><br/></li>
                    </ul>
                    <p></p><p>6. Jadvallar:</p><p></p>
                    <div align="left">
                        <ul>
                            <li><span>jadvallar ketma-ket raqamlanadi;</span></li>
                            <li><span>albatta jadval nomi va raqami bo‘lishi lozim;</span></li>
                            <li><span>maqolada “jadval” so‘zini to‘liq yozish lozim;</span></li>
                            <li><span>jadval nomi va uning tarkibi markaz bo‘yicha tekislanib yoziladi;</span></li>
                        </ul>
                    </div>
                    <p></p><p>7. Rasmlar (diagrammalar):</p><p></p>
                    <ul>
                        <li><span>rasmlar va diagrammalarni raqamlash ketma-ket amalga oshiriladi;</span><br/></li>
                        <li><span>albatta rasmlarni (diagrammalarni) nomi bo’lishi lozim;</span><br/></li>
                        <li><span>maqolada rasm va diagrammalar uchun rasm (<i>rasm, рис.</i>) so’zidan foydalanish lozim (ruschada <i>рисунок</i> deb to’liq yozish shart emas);</span><br/>
                        </li>
                        <li><span>rasmlar (diagrammalar) markazlashtiriladi va uning tagidan nomi yoziladi;</span><br/>
                        </li>
                        <li><span>Microsoft Wordda qilingan rasmlardan foydalanish kerak emas.</span><br/></li>
                    </ul>
                    <p></p><p>8. Qandaydir skaner qilib olingan ma’lumotlardan foydalanish taqiqlanadi (formulalar,
                    jadvallar, rasmlar va boshqalar).<br/></p><p>9. Maqola uchta qismga bo’linishi kerak:</p><p><b>I.
                    Kirish(Введение, Introduction)</b></p><p><b>II. Asosiy qism (Основная часть, Main part)</b></p><p>
                    <b>III. Xulosa (Заключение, Conclusion)</b></p><p>10. Maqola oxirida foydalanilgan adabiyotlar
                    ro’yxati keltiriladi.</p><p>Foydalanilgan adabiyotlarga ko’rsatkichlar kvadrat qavslar orqali
                    ko’rsatiladi. Masalan, [2]; [1-4, 6]. <br/></p><p>Foydalanilgan adabiyotlar maqolaning <b>ADABIYOTLAR
                    (ЛИТЕРАТУРА, REFERENCES)</b> nomli qismida keltiriladi.</p><p>Foydalanilgan adabiyotlar umum qabul
                    qilingan xalqaro me’yorlar asosida rasmiylashtiriladi.</p><p><b><i>Jurnal tahririyati bilan
                    aloqa</i></b></p><p><b>"TATU xabarlari” jurnali tahririyati manzili:</b></p><p className=""
                                                                                                   align="justify">100202,
                    O‘zbekiston Respublikasi, Toshkent shahri, Yunusobod tumani,<b> </b>Amir Temur ko‘chasi 108-uy,
                    TATU, A korpus, 2-etaj, 232-xona.<br/></p><p className=""><b>Qabul vaqti:</b> Har Chorshanba va
                    Payshanba soat 10:00 dan 16:00 gacha<br/></p><p><b>Telefon:</b> (+998-71) 238-65-99</p><p>
                    <b>E-mail:</b> <a href="mailto:tuit_xabar@tuit.uz" target="_blank">tuit_xabar@tuit.uz</a></p></div>
            </div>
        );
    }
}

Journal.propTypes = {};

export default Journal;