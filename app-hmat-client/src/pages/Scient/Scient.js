import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Link} from "react-router-dom";
import Magistr from "./Magistr";
import Doctarant from "./Doctarant";
import Journal from "./Journal";

class Scient extends Component {
    state={
        number:0
    }
    render() {
        const {number}=this.state
        const getPage=(n)=>{
            console.log(n)
            this.setState({number:n})
        }
        function SwitchPage(n){
            switch (n){
                case 0:return <Magistr/>
                case 1:return <Doctarant/>
                case 2:return <Journal/>
            }
        }
        return (
            <div>
                <div id="content" className="site-content">
                    <div className="page-title parallax-window" data-parallax="scroll"
                         data-image-src="assets/images/placeholder/event-title.jpg">
                        <div className="container">
                            <h1>Event Grid</h1>
                            <div className="breadcrumb">
                                <ul>
                                    <li><a href="/">Home</a></li>
                                    <li><span className="current">Ilmiy faoliyati</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="container">
                        <div className="row">
                            <main id="main" className="site-main col-md-12">
                                <div className="filter">
                                    <ul>
                                        <li><Link className="active" onClick={()=>getPage(0)}>Magistratura</Link></li>
                                        <li><Link onClick={()=>getPage(1)}>Doktorantura</Link></li>
                                        <li><Link onClick={()=>getPage(2)}>Ilmiy jurnal</Link></li>
                                    </ul>
                                </div>

                                {SwitchPage(number)}

                            </main>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

Scient.propTypes = {};

export default Scient;