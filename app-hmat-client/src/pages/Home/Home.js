import React, {Component} from 'react';
import {request} from "../utils/request";
import Carousel from 'react-elastic-carousel';
import {Link} from "react-router-dom";


class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            subjects: [],
            teachers: [],
            news:[]
        }

    }

    componentDidMount() {

        try{
            request({
                url: '/news/pageable',
                method: 'GET',
                data: {
                    page:0,
                    size:3
                }
            }).then(res=>{
                if(res.success){
                    console.log(res.content)
                    this.setState({news:res.content})
                }

            })

            request({
                url: '/subjects',
                method: 'GET'
            }).then(res => {
                if(res.success){
                    this.setState({
                        subjects: res.list
                    })
                }

            });
            request({
                url: '/teachers',
                method: 'GET'
            }).then(res => {
                if(res.success){
                    this.setState({teachers: res.list})
                }
            });
        }catch (e) {

        }



    }








    render() {
        const {teachers, subjects,news} = this.state;


        const breakPoints=[
            {width:1,itemsToShow:1},
            {width: 550,itemsToShow: 2},
            {width: 800,itemsToShow: 3},
            {width: 968,itemsToShow: 4},
            {width: 1200,itemsToShow: 5}
        ]
        const getNews=(id)=>{
            console.log(id)
            localStorage.setItem("singleNews",id);
        }

        return (
            <div className="home5 header5">
                <div id="pageloader">
                    <div className="pageloader">
                        <div className="thecube">
                            <div className="cube c1"></div>
                            <div className="cube c2"></div>
                            <div className="cube c4"></div>
                            <div className="cube c3"></div>
                        </div>

                        <div className="textedit">
                            <span className="site-name">HMAT</span>
                            <span className="site-tagline">HOME</span>
                        </div>
                    </div>

                </div>


                <div id="wrapper">

                    <div id="content" className="site-content">
                        <main id="main" className="site-main">
                            <section className="main-slider">
                                <div className="slider">
                                    <div className="item">
                                        <img src="assets/images/placeholder/slider8.jpg" alt=""/>
                                        <div className="container">
                                            <div className="slider-content">
                                                <div className="desc">
                                                    <h2>National University of Uzbekistan </h2>

                                                </div>
                                                <div className="button-action">
                                                    <a href="#" className="button primary rounded large">Hozir
                                                        murojaat qiling</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div className="item">
                                        <img src="assets/images/placeholder/slider9.jpg" alt=""/>
                                        <div className="container">
                                            <div className="slider-content">
                                                <div className="desc">
                                                    <h2>Ta'lim</h2>
                                                    <p>Ta'lim haqida qisqacha ma'lumot yoki yangiliklar.Ta'lim
                                                        haqida qisqacha ma'lumot
                                                        yoki yangiliklar.
                                                        Ta'lim haqida qisqacha ma'lumot yoki yangiliklar.Ta'lim
                                                        haqida qisqacha ma'lumot
                                                        yoki yangiliklar.
                                                        Ta'lim haqida qisqacha ma'lumot yoki yangiliklar.Ta'lim
                                                        haqida qisqacha ma'lumot
                                                        yoki yangiliklar.</p>
                                                </div>
                                                <div className="button-action">
                                                    <a href="#"
                                                       className="button primary rounded large">Batafsil</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div className="item">
                                        <img src="assets/images/placeholder/slider10.jpg" alt=""/>
                                        <div className="container">
                                            <div className="slider-content">
                                                <div className="desc">
                                                    <h2>Kafedra haqida</h2>
                                                    <p>Kafedra haqida qisqacha ma'lumot.Kafedra haqida qisqacha
                                                        ma'lumot.Kafedra haqida
                                                        qisqacha ma'lumot.
                                                        Kafedra haqida qisqacha ma'lumot.Kafedra haqida qisqacha
                                                        ma'lumot.Kafedra haqida
                                                        qisqacha ma'lumot</p>
                                                </div>
                                                <div className="button-action">
                                                    <a href="#"
                                                       className="button primary rounded large">Batafsil</a>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </div>

                            </section>

                            <section id="university-courses-3"  >
                                <div className="text-center" style={{maxWidth:'1300px',marginLeft:20}}>
                                    <h2 className="main-title ribbon"><span>Fanlar</span></h2>
                                    <p className="center">Hisoblash matematikasi va axborot tizimlari kafedrasi
                                        professor o'qituvchilari
                                        tomonidan o'tiladigan fanlar.</p>
                                    <Carousel breakPoints={breakPoints} enableAutoPlay={true} autoPlaySpeed={3000}>
                                            {subjects.length > 0 ?
                                                subjects.map((item, i) =>
                                                    <div key={i} className="course" style={{width:'280px'}} >
                                                        <div className="course-inner shadow">
                                                            <div className="course-thumb">
                                                                <a className="mini-zoom" href="single-course.html">
                                                                    <img style={{width:'280px',height:'180px'}} src={`/avatar/download/name/${item.img}`}
                                                                         alt=""/>

                                                                </a>
                                                            </div>

                                                            <div className="course-info" style={{height:'20vh'}}>
                                                                    <span className="course-cat"><a
                                                                        href="#">{item.name.uz}</a></span>


                                                            </div>
                                                            <div className="course-author text-center " style={{bottom:'0'}}>
                                                                {item.teachersImg.map((item,i)=>
                                                                    <a key={i} href="#"><img style={{width:40,height:50}}
                                                                        src={`/avatar/download/name/${item}`}
                                                                        alt=""/></a>
                                                                )}

                                                            </div>

                                                        </div>
                                                    </div>


                                                )

                                                : <h1>Data not found</h1>
                                            }
                                    </Carousel>
                                </div>

                            </section>

                            <section id="home-blog">
                                <div className="container text-center">
                                    <h2 className="main-title ribbon"><span><Link to="/news" style={{color:'white'}}>YAQINLASHAYOTGAN VOQEALAR</Link></span></h2>
                                    <p className="center">Qisqacha tafsif. Qisqacha tafsif. Qisqacha tafsif. Qisqacha
                                        tafsif. Qisqacha
                                        tafsif. Qisqacha tafsif</p>

                                        <div className="events layout column-3 isotope">
                                            {news.length > 0 ?
                                                news.map((item, i) =>
                                                    <div key={i} className="event isotope-item paid">
                                                        <div className="event-inner shadow">
                                                            <div className="event-thumb">
                                                                <a className="mini-zoom" href="">
                                                                    <img style={{width: 327, height: 198}}
                                                                         src={`/avatar/download/name/${item.avatar}`}
                                                                         alt=""/></a>
                                                            </div>

                                                            <div className="event-date">
                                                                <span>{new Date(item.createdDate).getDay()}</span>{new Date(item.createdDate).toDateString().slice(4, 7)}
                                                            </div>

                                                            <div className="event-info" style={{height: '23vh'}}>
                                                                <h3 className="post-title">
                                                                    <Link to="/single" onClick={()=>getNews(item.id)} >{item.title.uz}</Link>
                                                                </h3>

                                                            </div>
                                                        </div>
                                                    </div>
                                                )


                                                : <h1>Data Not Found</h1>}

                                        </div>


                                    <a href="/news" className="button  large rounded see-more-btn">See more events</a>
                                </div>
                            </section>




                            <section id="call-to-action" className="call-to-action parallax-window"
                                     data-parallax="scroll"
                                     data-image-src="assets/images/placeholder/call-to-action.jpg">
                                <div className="container">
                                    <div className="call-to-action-content">
                                        <h2>KAFEDRA MAVZULARI</h2>
                                        <div className="desc">Muvaffaqiyatingizni ustuvor yo'nalishga aylantiring</div>
                                        <a href="#"
                                           className="button primary rounded large animation-element fade-left">Talabalar
                                            hayoti</a>

                                    </div>
                                </div>

                            </section>

                            <section id="home-professor">
                                <div className="text-center" style={{maxWidth:'1300px',marginLeft:20}}>
                                    <h2 className="main-title ribbon"><span>KAFEDRAMIZNI PROFESSOR O'QITUVCHILARI</span>
                                    </h2>
                                    <p className="center">Qisqa bitta ma'noli gap. Qisqa bitta ma'noli gap. Qisqa bitta
                                        ma'noli gap.</p>

                                    <Carousel breakPoints={breakPoints} enableAutoPlay={true} autoPlaySpeed={3000}>
                                        {teachers.length > 0 ?
                                            teachers.map((item, i) =>
                                                <div key={i} className="team box" style={{width:'280px'}}>
                                                    <div className="thumb">
                                                        <img style={{width:'250px',height:'48vh'}} src={`/avatar/download/name/${item.img}`} alt=""/>
                                                        <div className="thumb-info" >
                                                            <div className="socials">
                                                                <ul>
                                                                    <li><a  target="_blank"><i
                                                                        className="fa fa-envelope"></i></a>
                                                                    </li>
                                                                    <li><a  target="_blank"><i
                                                                        className="fa fa-facebook"></i></a>
                                                                    </li>
                                                                    <li><a  target="_blank"><i
                                                                        className="fa fa-linkedin "></i></a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <a  className="read-more">Read More</a>
                                                        </div>
                                                    </div>


                                                    <div className="info" style={{height:'18vh'}}>
                                                        <span className="name">{item.lastName.uz+' '+item.firstname.uz}</span>
                                                        <span className="job">{item.degree.uz}</span>

                                                    </div>
                                                    <div><p>{item.content.uz}</p></div>
                                                </div>
                                            )
                                            : <h1>Data Not found</h1>
                                        }
                                    </Carousel>


                                </div>

                            </section>


                            <section id="home-partner">
                                <div className="container">
                                    <h2 className="main-title ribbon"><span>Xalqaro aloqalar</span></h2>
                                    <div className="carousel animation-element fade-in">
                                        <div className="slider">
                                            <div className="item" style={{marginLeft: '20px'}}>
                                                <a href="#"><img src="assets/images/placeholder/mara.jpg" alt=""/></a>
                                            </div>
                                            <div className="item" style={{marginLeft: '20px'}}>
                                                <a href="#"><img src="assets/images/placeholder/mara.jpg" alt=""/></a>
                                            </div>
                                            <div className="item" style={{marginLeft: '20px'}}>
                                                <a href="#"><img src="assets/images/placeholder/mara.jpg" alt=""/></a>
                                            </div>
                                            <div className="item" style={{marginLeft: '20px'}}>
                                                <a href="#"><img src="assets/images/placeholder/mara.jpg" alt=""/></a>
                                            </div>
                                            <div className="item" style={{marginLeft: '20px'}}>
                                                <a href="#"><img src="assets/images/placeholder/mara.jpg" alt=""/></a>
                                            </div>
                                            <div className="item" style={{marginLeft: '20px'}}>
                                                <a href="#"><img src="assets/images/placeholder/mara.jpg" alt=""/></a>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                            </section>


                            <section id="home-map">
                                <div id="map"></div>
                            </section>

                        </main>
                    </div>


                </div>

            </div>
        );
    }
}

Home.propTypes = {};


export default Home;