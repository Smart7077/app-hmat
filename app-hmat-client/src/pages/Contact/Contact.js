import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { request} from "../utils/request";


class Contact extends Component {
    render() {


        return (
            <div>
                <div className="container">
                    <div className="row">
                        <main id="main" className="site-main col-md-12">
                            <div className="row">
                                <div className="col-md-6">
                                    <div className="contact-form">
                                        <h3 className="title">Contact Form</h3>
                                        <p>Send an email. All fields with an * are required.</p>
                                        <form action="#" method="get">
                                            <input className="input-text" type="text" placeholder="Your Name *"/>
                                                <input className="input-text" type="email" placeholder="Your Email *"/>
                                                    <div className="selectbox">
                                                        <select>
                                                            <option>-- Select subjects *</option>
                                                            <option value="subjects one">Subjects one</option>
                                                            <option value="subjects two">Subjects two</option>
                                                        </select>
                                                    </div>
                                                    <textarea rows="2" cols="50"
                                                              placeholder="Your Message *"></textarea>
                                                    <div className="checkbox">
                                                        <input id="send" type="checkbox" value="1"/>
                                                            <label htmlFor="send">Send copy to yourself</label>
                                                    </div>
                                                    <input className="button primary rounded" type="submit"
                                                           value="Send Email"/>
                                        </form>
                                    </div>
                                </div>

                                <div className="col-md-6">
                                    <div className="contact-info">
                                        <div id="map"></div>

                                        <h3 className="title">Contact Infomation</h3>
                                        <p>Address: 273 5th Ave New York, NYC 10018, United States</p>
                                        <p>Phone: +(112) 345 6879</p>
                                        <p>Fax: +(112) 345 8796</p>
                                        <p>Email: <a href="mailto:contact@university.com">contact@university.com</a></p>
                                    </div>
                                </div>
                            </div>
                        </main>
                    </div>
                </div>
            </div>
        );
    }
}

Contact.propTypes = {};

export default Contact;