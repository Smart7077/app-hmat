package uz.nuu.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.nuu.entity.Role;

import java.util.List;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

    Role getBySystemName(String systemName);

    List<Role> findAllByIdIn(List<Long> ids);
}
