package uz.nuu.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.nuu.entity.Words;

@Repository
public interface WordsRepository extends JpaRepository<Words, Long> {
}
