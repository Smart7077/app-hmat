package uz.nuu.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.nuu.entity.BooksType;

import java.util.UUID;

@Repository
public interface BooksTypeRepository extends JpaRepository<BooksType, UUID> {
}
