package uz.nuu.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.nuu.entity.Articles;
import uz.nuu.entity.Teachers;

import java.util.List;
import java.util.UUID;

@Repository
public interface TeachersRepository extends JpaRepository<Teachers, UUID> {
    List<Teachers> findAllByIdIn(List<UUID> idList);
}
