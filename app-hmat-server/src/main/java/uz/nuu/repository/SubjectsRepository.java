package uz.nuu.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.nuu.entity.Subjects;

import java.util.UUID;

@Repository
public interface SubjectsRepository extends JpaRepository<Subjects, UUID> {
}
