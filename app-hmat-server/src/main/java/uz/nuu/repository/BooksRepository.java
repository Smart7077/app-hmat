package uz.nuu.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.nuu.entity.Books;

import java.util.UUID;

@Repository
public interface BooksRepository extends JpaRepository<Books, UUID> {
}
