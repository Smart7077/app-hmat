package uz.nuu.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.nuu.entity.FileDocumentTypeForFileDocumentsEntity;

@Repository
public interface FileDocumentTypeForFileDocumentsRepository extends JpaRepository<FileDocumentTypeForFileDocumentsEntity, String> {
}
