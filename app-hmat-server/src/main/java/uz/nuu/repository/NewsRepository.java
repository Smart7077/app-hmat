package uz.nuu.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.nuu.entity.News;
import uz.nuu.entity.Permission;

import java.util.HashMap;
import java.util.UUID;

@Repository
public interface NewsRepository extends JpaRepository<News, UUID> {
    News findByTitle(HashMap<String, String> title);

    Page<News> findAllByCategoryIdIs(UUID uuid, Pageable pageable);
}
