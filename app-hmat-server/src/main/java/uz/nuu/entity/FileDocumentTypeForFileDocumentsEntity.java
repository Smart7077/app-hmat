package uz.nuu.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "file_document_type_for_file_documents")
public class FileDocumentTypeForFileDocumentsEntity {

    @Id
    @Column(name = "name", nullable = false, insertable = false, unique = true)
    private String name;

    public FileDocumentTypeForFileDocumentsEntity() {
    }

    public FileDocumentTypeForFileDocumentsEntity(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "FileDocumentTypeForFileDocumentsEntity{" +
                "name='" + name + '\'' +
                '}';
    }
}
