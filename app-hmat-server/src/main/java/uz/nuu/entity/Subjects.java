package uz.nuu.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.util.Collection;
import java.util.HashMap;
import java.util.UUID;

@Entity
@Table(name = "subjects")
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
public class Subjects {

    @Id
    @GeneratedValue
    private UUID id;

    @Column(name = "name", columnDefinition = "jsonb")
    @Type(type = "jsonb")
    private HashMap<String, String> name;

    @Column(name = "title", columnDefinition = "jsonb")
    @Type(type = "jsonb")
    private HashMap<String, String> title;

    @Column(name = "content", columnDefinition = "jsonb")
    @Type(type = "jsonb")
    private HashMap<String, String> content;

    @Column(name = "img")
    private String img;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "teachers_subjects",
            joinColumns = @JoinColumn(name = "teacher_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "subject_id", referencedColumnName = "id")
    )
    @OrderBy
    @JsonIgnore
    private Collection<Teachers> teachersSubjects;

    public Subjects() {
    }

    public Subjects(HashMap<String, String> name, HashMap<String, String> title, HashMap<String, String> content, String img, Collection<Teachers> teachersSubjects) {
        this.name = name;
        this.title = title;
        this.content = content;
        this.img = img;
        this.teachersSubjects = teachersSubjects;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public HashMap<String, String> getName() {
        return name;
    }

    public void setName(HashMap<String, String> name) {
        this.name = name;
    }

    public HashMap<String, String> getTitle() {
        return title;
    }

    public void setTitle(HashMap<String, String> title) {
        this.title = title;
    }

    public HashMap<String, String> getContent() {
        return content;
    }

    public void setContent(HashMap<String, String> content) {
        this.content = content;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public Collection<Teachers> getTeachersSubjects() {
        return teachersSubjects;
    }

    public void setTeachersSubjects(Collection<Teachers> teachersSubjects) {
        this.teachersSubjects = teachersSubjects;
    }

    @Override
    public String toString() {
        return "Subjects{" +
                "id=" + id +
                ", name=" + name +
                ", title=" + title +
                ", content=" + content +
                ", img='" + img + '\'' +
                ", teachersSubjects=" + teachersSubjects +
                '}';
    }
}