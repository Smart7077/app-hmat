package uz.nuu.entity;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.util.HashMap;
import java.util.UUID;

@Entity
@Table(name = "students")
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
public class Students {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private UUID id;

    @Column(name = "firstname")
    @Type(type = "jsonb")
    private HashMap<String, String> firstName;

    @Column(name = "lastname")
    @Type(type = "jsonb")
    private HashMap<String, String> lastName;

    @Column(name = "middleName")
    @Type(type = "jsonb")
    private HashMap<String, String> middleName;

    @Column(name = "address")
    @Type(type = "jsonb")
    private HashMap<String, String> address;

    @Column(name = "direction")
    @Type(type = "jsonb")
    private HashMap<String, String> direction;

    @Column(name = "course")
    private Long course;

    @Column(name = "url_image")
    private String urlImage;

    @Column(name = "reading_type")
    @Type(type = "jsonb")
    private HashMap<String, String> readingType;

    @Column(name = "degree_type")
    @Type(type = "jsonb")
    private HashMap<String, String> degreeType;

    @Column(name = "email")
    private String email;

    @Column(name = "facebook")
    private String facebook;

    @Column(name = "linked_in")
    private String linkedIn;

    @Column(name = "twitter")
    private String twitter;

    @Column(name = "instagram")
    private String instagram;

    public Students() {
    }

    public Students(HashMap<String, String> firstName, HashMap<String, String> lastName, HashMap<String, String> middleName, HashMap<String, String> address, HashMap<String, String> direction, Long course, String urlImage, HashMap<String, String> readingType, HashMap<String, String> degreeType, String email, String facebook, String linkedIn, String twitter, String instagram) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.address = address;
        this.direction = direction;
        this.course = course;
        this.urlImage = urlImage;
        this.readingType = readingType;
        this.degreeType = degreeType;
        this.email = email;
        this.facebook = facebook;
        this.linkedIn = linkedIn;
        this.twitter = twitter;
        this.instagram = instagram;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public HashMap<String, String> getFirstName() {
        return firstName;
    }

    public void setFirstName(HashMap<String, String> firstName) {
        this.firstName = firstName;
    }

    public HashMap<String, String> getLastName() {
        return lastName;
    }

    public void setLastName(HashMap<String, String> lastName) {
        this.lastName = lastName;
    }

    public HashMap<String, String> getMiddleName() {
        return middleName;
    }

    public void setMiddleName(HashMap<String, String> middleName) {
        this.middleName = middleName;
    }

    public HashMap<String, String> getAddress() {
        return address;
    }

    public void setAddress(HashMap<String, String> address) {
        this.address = address;
    }

    public HashMap<String, String> getDirection() {
        return direction;
    }

    public void setDirection(HashMap<String, String> direction) {
        this.direction = direction;
    }

    public Long getCourse() {
        return course;
    }

    public void setCourse(Long course) {
        this.course = course;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public HashMap<String, String> getReadingType() {
        return readingType;
    }

    public void setReadingType(HashMap<String, String> readingType) {
        this.readingType = readingType;
    }

    public HashMap<String, String> getDegreeType() {
        return degreeType;
    }

    public void setDegreeType(HashMap<String, String> degreeType) {
        this.degreeType = degreeType;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getLinkedIn() {
        return linkedIn;
    }

    public void setLinkedIn(String linkedIn) {
        this.linkedIn = linkedIn;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    @Override
    public String toString() {
        return "Students{" +
                "id=" + id +
                ", firstName=" + firstName +
                ", lastName=" + lastName +
                ", middleName=" + middleName +
                ", address=" + address +
                ", direction=" + direction +
                ", course=" + course +
                ", urlImage='" + urlImage + '\'' +
                ", readingType=" + readingType +
                ", degreeType=" + degreeType +
                ", email='" + email + '\'' +
                ", facebook='" + facebook + '\'' +
                ", linkedIn='" + linkedIn + '\'' +
                ", twitter='" + twitter + '\'' +
                ", instagram='" + instagram + '\'' +
                '}';
    }
}
