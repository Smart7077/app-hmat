package uz.nuu.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "file_avatar", indexes = {@Index(columnList = "id")})
@SQLDelete(sql = " update file_avatar set deleted = 'true' where id = ?")
@Where(clause = "deleted = 'false'")
public class FileAvatar {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private UUID id;

    @Column(name = "orginal_name")
    private String originalName;

    @Column(name = "generated_name")
    private String generatedName;

    @CreatedBy
    @Column(name = "created_by", length = 50, updatable = false)
    @JsonIgnore
    private String createdBy;

    @CreatedDate
    @JsonIgnore
    @Column(name = "created_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate = new Date(System.currentTimeMillis());

    @LastModifiedDate
    @Column(name = "modified_date")
    @JsonIgnore
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedDate;

    @LastModifiedBy
    @Column(name = "modified_by", length = 50)
    @JsonIgnore
    private String modifiedBy;

    @Column(name = "deleted", columnDefinition = " boolean DEFAULT false")
    @JsonIgnore
    private boolean deleted = false;


    public FileAvatar() {
    }

    public FileAvatar(UUID id, String originalName, String generatedName) {
        this.id = id;
        this.originalName = originalName;
        this.generatedName = generatedName;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getOriginalName() {
        return originalName;
    }

    public void setOriginalName(String originalName) {
        this.originalName = originalName;
    }

    public String getGeneratedName() {
        return generatedName;
    }

    public void setGeneratedName(String generatedName) {
        this.generatedName = generatedName;
    }

    @Override
    public String toString() {
        return "FileAvatar{" +
                "id=" + id +
                ", originalName='" + originalName + '\'' +
                ", generatedName='" + generatedName + '\'' +
                '}';
    }
}
