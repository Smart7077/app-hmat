package uz.nuu.entity;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

@Entity
@Table(name = "teachers")
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
public class Teachers {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private UUID id;

    @Column(name = "firstname", columnDefinition = "jsonb")
    @Type(type = "jsonb")
    private HashMap<String, String> firstname;

    @Column(name = "lastname", columnDefinition = "jsonb")
    @Type(type = "jsonb")
    private HashMap<String, String> lastName;

    @Column(name = "middlename", columnDefinition = "jsonb")
    @Type(type = "jsonb")
    private HashMap<String, String> middleName;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "address", columnDefinition = "jsonb")
    @Type(type = "jsonb")
    private HashMap<String, String> address;

    @Column(name = "img")
    private String img;

    @Column(name = "email")
    private String email;

    @Column(name = "facebook")
    private String facebook;

    @Column(name = "linked_in")
    private String linkedIn;

    @Column(name = "instagram")
    private String instagram;

    @Column(name = "telegram")
    private String telegram;

    @Column(name = "degree", columnDefinition = "jsonb")
    @Type(type = "jsonb")
    private HashMap<String, String> degree;

    @Column(name = "content", columnDefinition = "jsonb")
    @Type(type = "jsonb")
    private HashMap<String, String> content;

    @Column(name = "created_date")
    private Date createdDate;

    @Column(name = "modifiedDate")
    private Date modifiedDate;

    @Column(name = "createdBy")
    private String createdBy;

    @Column(name = "modified_by")
    private String modifiedBy;

    @Column(name = "status")
    private Boolean status;

    public Teachers() {
    }

    public Teachers(HashMap<String, String> firstname, HashMap<String, String> lastName, HashMap<String, String> middleName, String phoneNumber, HashMap<String, String> address, String img, String email, String facebook, String linkedIn, String instagram, String telegram, HashMap<String, String> degree, HashMap<String, String> content, Boolean status) {
        this.firstname = firstname;
        this.lastName = lastName;
        this.middleName = middleName;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.img = img;
        this.email = email;
        this.facebook = facebook;
        this.linkedIn = linkedIn;
        this.instagram = instagram;
        this.telegram = telegram;
        this.degree = degree;
        this.content = content;
        this.status = status;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public HashMap<String, String> getFirstname() {
        return firstname;
    }

    public void setFirstname(HashMap<String, String> firstname) {
        this.firstname = firstname;
    }

    public HashMap<String, String> getLastName() {
        return lastName;
    }

    public void setLastName(HashMap<String, String> lastName) {
        this.lastName = lastName;
    }

    public HashMap<String, String> getMiddleName() {
        return middleName;
    }

    public void setMiddleName(HashMap<String, String> middleName) {
        this.middleName = middleName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public HashMap<String, String> getAddress() {
        return address;
    }

    public void setAddress(HashMap<String, String> address) {
        this.address = address;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getLinkedIn() {
        return linkedIn;
    }

    public void setLinkedIn(String linkedIn) {
        this.linkedIn = linkedIn;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagramm) {
        this.instagram = instagramm;
    }

    public String getTelegram() {
        return telegram;
    }

    public void setTelegram(String telegram) {
        this.telegram = telegram;
    }

    public HashMap<String, String> getDegree() {
        return degree;
    }

    public void setDegree(HashMap<String, String> degree) {
        this.degree = degree;
    }

    public HashMap<String, String> getContent() {
        return content;
    }

    public void setContent(HashMap<String, String> content) {
        this.content = content;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Teachers{" +
                "id=" + id +
                ", firstname=" + firstname +
                ", lastName=" + lastName +
                ", middleName=" + middleName +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", address=" + address +
                ", img='" + img + '\'' +
                ", email='" + email + '\'' +
                ", facebook='" + facebook + '\'' +
                ", linkedIn='" + linkedIn + '\'' +
                ", instagram='" + instagram + '\'' +
                ", telegram='" + telegram + '\'' +
                ", degree=" + degree +
                ", content='" + content + '\'' +
                ", createdDate=" + createdDate +
                ", modifiedDate=" + modifiedDate +
                ", createdBy='" + createdBy + '\'' +
                ", modifiedBy='" + modifiedBy + '\'' +
                ", status=" + status +
                '}';
    }
}
