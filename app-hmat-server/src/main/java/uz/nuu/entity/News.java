package uz.nuu.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

@Entity
@Table(name = "news")
@TypeDef(name = "json", typeClass = JsonBinaryType.class)
public class News {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private UUID id;

    @Column(name = "title", columnDefinition = "jsonb")
    @Type(type = "jsonb")
    private HashMap<String, String> title;

    @Column(name = "content", columnDefinition = "jsonb")
    @Type(type = "jsonb")
    private HashMap<String, String> content;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "category_id", updatable = false, insertable = false, nullable = false)
    private Category category;

    @Column(name = "category_id")
    private UUID categoryId;

    @Column(name = "status")
    private Boolean status;

    @Column(name = "avatar")
    private String avatar;

    @CreatedBy
    @Column(name = "created_by", length = 50, updatable = false)
    @JsonIgnore
    private String createdBy;

    @CreatedDate
    @JsonIgnore
    @Column(name = "created_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate = new Date(System.currentTimeMillis());

    @LastModifiedDate
    @Column(name = "modified_date")
    @JsonIgnore
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedDate;

    @LastModifiedBy
    @Column(name = "modified_by", length = 50)
    @JsonIgnore
    private String modifiedBy;

    @Column(name = "deleted", columnDefinition = " boolean DEFAULT false")
    @JsonIgnore
    private boolean deleted = false;

    public News() {
    }

    public News(HashMap<String, String> title, HashMap<String, String> content , Category category, UUID categoryId, Boolean status, String avatar, String createdBy, Date createdDate, Date modifiedDate, String modifiedBy, boolean deleted) {
        this.title = title;
        this.content = content;
        this.category = category;
        this.categoryId = categoryId;
        this.status = status;
        this.avatar = avatar;
        this.createdBy = createdBy;
        this.createdDate = createdDate;
        this.modifiedDate = modifiedDate;
        this.modifiedBy = modifiedBy;
        this.deleted = deleted;
    }

    public News(HashMap<String, String> title,  HashMap<String, String> content, Category category, UUID categoryId, Boolean status, String avatar, boolean deleted) {
        this.title = title;
        this.content = content;
        this.category = category;
        this.categoryId = categoryId;
        this.status = status;
        this.avatar = avatar;
        this.deleted = deleted;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public HashMap<String, String> getTitle() {
        return title;
    }

    public void setTitle(HashMap<String, String> title) {
        this.title = title;
    }

    public HashMap<String, String> getContent() {
        return content;
    }

    public void setContent(HashMap<String, String> content) {
        this.content = content;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public UUID getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(UUID categoryId) {
        this.categoryId = categoryId;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "News{" +
                "id=" + id +
                ", title=" + title +
                ", content='" + content + '\'' +
                ", category=" + category +
                ", categoryId=" + categoryId +
                ", status=" + status +
                ", avatar='" + avatar + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", createdDate=" + createdDate +
                ", modifiedDate=" + modifiedDate +
                ", modifiedBy='" + modifiedBy + '\'' +
                ", deleted=" + deleted +
                '}';
    }
}
