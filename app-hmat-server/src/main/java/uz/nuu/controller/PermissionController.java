package uz.nuu.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.nuu.converter.PermissionConverter;
import uz.nuu.entity.Permission;
import uz.nuu.exception.ApiException;
import uz.nuu.exception.DataAlreadyExistsException;
import uz.nuu.exception.DataNotFoundException;
import uz.nuu.payload.ErrorResponse;
import uz.nuu.payload.PermissionRequest;
import uz.nuu.payload.PermissionResponse;
import uz.nuu.service.PermissionService;

@RestController
@RequestMapping(value = "/v1/permission")
@Api(value = "Permission endpoint")
public class PermissionController {

    private static final String CREATE = "/create";
    private static final String GET_BY_ID = "/{id}";
    private static final String GET_ALL_PAGEABLE = "/pageable";
    private static final String GET_ALL = "";
    private static final String UPDATE = "/update/{id}";
    private static final String DELETE = "/{id}";

    private static final Logger logger = LogManager.getLogger("ServiceLogger");
    private final PermissionService permissionService;
    private final PermissionConverter permissionConverter;

    public PermissionController(PermissionService permissionService) {
        this.permissionService = permissionService;
        this.permissionConverter = new PermissionConverter();
    }

    @PostMapping(CREATE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully created permission"),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorResponse.class)})
    public ResponseEntity<PermissionResponse> create(@RequestBody PermissionRequest request) throws DataAlreadyExistsException {
        logger.info("Create new Permission : " + request);
        PermissionResponse response = permissionConverter.convertFromEntity(
                permissionService.create(permissionConverter.convertFromRequest(request))
        );
        return ResponseEntity.ok(response);
    }

    @GetMapping(GET_BY_ID)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found", response = ErrorResponse.class),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorResponse.class)})
    public ResponseEntity<PermissionResponse> getById(@PathVariable Long id) throws DataNotFoundException {
        logger.info("Get permission by id: " + id);
        return ResponseEntity.ok(
                permissionConverter.convertFromEntity(
                        permissionService.getById(id)
                )
        );
    }

    @GetMapping(GET_ALL_PAGEABLE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found", response = ErrorResponse.class),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorResponse.class)})
    public ResponseEntity getAllPageable(@RequestParam int page, @RequestParam int size) throws ApiException {
        logger.info("get all permission with page: " + page + " and size :" + size);
        return ResponseEntity.ok(
                permissionConverter.convertFromEntities(
                        permissionService.list(PageRequest.of(page, size))
                )
        );
    }

    @GetMapping(GET_ALL)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found", response = ErrorResponse.class),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorResponse.class)})
    public ResponseEntity getAll() throws ApiException {
        logger.info("Get all permission");
        return ResponseEntity.ok(
                permissionConverter.convertFromEntitiesList(
                        permissionService.list()
                )
        );
    }

    @PutMapping(UPDATE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found", response = ErrorResponse.class),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorResponse.class)})
    public ResponseEntity update(@RequestBody PermissionRequest request, @PathVariable Long id) throws DataNotFoundException, DataAlreadyExistsException {
        Permission permission = permissionConverter.convertFromRequest(request);
        logger.info("update permission with this param : " + request);
        return ResponseEntity.ok(
                permissionConverter.convertFromEntity(permissionService.update(id, permission))
        );
    }

    @DeleteMapping(DELETE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found", response = ErrorResponse.class),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorResponse.class)})
    public ResponseEntity delete(@PathVariable Long id) throws ApiException {
        logger.info("delete permission with this id" + id);
        return ResponseEntity.ok(permissionService.delete(id));
    }
}
