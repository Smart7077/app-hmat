package uz.nuu.controller;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.nuu.converter.SubjectsConverter;
import uz.nuu.entity.Subjects;
import uz.nuu.entity.Teachers;
import uz.nuu.exception.ApiException;
import uz.nuu.exception.DataAlreadyExistsException;
import uz.nuu.exception.DataNotFoundException;
import uz.nuu.payload.SubjectsRequest;
import uz.nuu.service.SubjectsService;
import uz.nuu.service.TeachersService;

import java.util.Collection;
import java.util.UUID;

@RestController
@RequestMapping("/subjects")
public class SubjectsController {
    private static final String CREATE = "/create";
    private static final String GET_BY_ID = "/{id}";
    private static final String GET_ALL = "";
    private static final String UPDATE = "/update/{id}";
    private static final String DELETE = "/{id}";

    private final SubjectsService subjectsService;
    private final TeachersService teachersService;
    private final SubjectsConverter subjectsConverter;

    public SubjectsController(SubjectsService subjectsService, TeachersService teachersService) {
        this.subjectsService = subjectsService;
        this.teachersService = teachersService;
        this.subjectsConverter = new SubjectsConverter();
    }

    private static final Logger logger = LogManager.getLogger("ControllerLogger");

    @PostMapping(CREATE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully created permission"),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorController.class)})
    public ResponseEntity create(@RequestBody SubjectsRequest request) throws ApiException {
        logger.info("Create new subjects : " + request);
        Collection<Teachers> teachersCollection = null;
        if (request.getTeachersIdList() != null) {
            teachersCollection = teachersService.findAllByIdIn(request.getTeachersIdList());
        }
        return ResponseEntity.ok(subjectsService.create(subjectsConverter.convertFromRequest(request, teachersCollection)));
    }

    @GetMapping(GET_BY_ID)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully getById subjects"),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorController.class)})
    public ResponseEntity getById(@PathVariable UUID id) throws ApiException {
        logger.info("Get subjects by this id : " + id);
        return ResponseEntity.ok(subjectsService.getById(id));
    }

    @GetMapping(GET_ALL)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully get all words"),
            @ApiResponse(code = 500, message = "Something went error", response = ErrorController.class)})
    public ResponseEntity getAll() throws ApiException {
        logger.info("Get all words : ");
        return ResponseEntity.ok(subjectsConverter.convertFromEntitiesList(subjectsService.list()));
    }

    @PutMapping(UPDATE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully words update"),
            @ApiResponse(code = 500, message = "Something went error", response = ErrorController.class)})
    public ResponseEntity update(@RequestBody Subjects request, @PathVariable UUID id) throws DataNotFoundException, DataAlreadyExistsException {
        logger.info("Update words by this request : " + request + "id = " + id);
        return ResponseEntity.ok(
                subjectsService.update(request, id)
        );
    }

    @DeleteMapping(DELETE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully delete subjects"),
            @ApiResponse(code = 500, message = "Something went error", response = ErrorController.class)})
    public ResponseEntity delete(@PathVariable UUID id) throws DataNotFoundException, DataAlreadyExistsException {
        logger.info("Delete role by this id : " + id);
        return ResponseEntity.ok(subjectsService.delete(id));
    }
}
