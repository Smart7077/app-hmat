package uz.nuu.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import uz.nuu.converter.FileDocumentsConverter;
import uz.nuu.exception.DataNotFoundException;
import uz.nuu.exception.FileUploadException;
import uz.nuu.exception.InvalidDataException;
import uz.nuu.payload.ErrorResponse;
import uz.nuu.payload.FileDocumentsResponse;
import uz.nuu.service.FileDocumentsService;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/file/documents")
public class FileDocumentsController {


    private static final String UPLOAD = "/upload";
    private static final String DOWNLOAD_BY_DOC_ID = "/download/{documentId}";
    private static final String DOWNLOAD_BY_GENERATED_NAME = "/download/name/{docGeneratedName}";
    private static final String GET_FILE_INFO_BY_GENERATED_NAME = "/get/info/{docGeneratedName}";

    private final FileDocumentsService fileDocumentsService;

    private FileDocumentsConverter fileDocumentsConverter;

    public FileDocumentsController(FileDocumentsService fileDocumentsService) {
        this.fileDocumentsService = fileDocumentsService;
        this.fileDocumentsConverter = new FileDocumentsConverter();
    }

    @PostMapping(value = UPLOAD, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Add new  documents")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successful"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found", response = ErrorResponse.class),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorResponse.class)})
    public ResponseEntity<List<FileDocumentsResponse>> upload(
            @RequestParam("files") MultipartFile[] multipartFiles,
            @RequestParam("documentsType") String documentsType) throws FileUploadException, InvalidDataException {

        List<MultipartFile> files = new ArrayList<>(Arrays.asList(multipartFiles));

        return ResponseEntity.ok(
                fileDocumentsConverter
                        .createFromEntities(
                                fileDocumentsService
                                        .documentsUpload(
                                                files,
                                                documentsType)));
    }

    @GetMapping(value = DOWNLOAD_BY_DOC_ID)
    @ApiOperation(value = "Download documents by Id")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successful"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found", response = ErrorResponse.class),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorResponse.class)})
    public ResponseEntity<Resource> download(@PathVariable("documentId") UUID documentId, HttpServletRequest request) throws DataNotFoundException, InvalidDataException, MalformedURLException {

        Resource resource = fileDocumentsService.getDocumentAsResourceById(documentId);

        String contentType = null;

        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            ex.printStackTrace();
            //throw new InvalidDataException("Invalid Data");
        }

        if (contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }

    @GetMapping(value = DOWNLOAD_BY_GENERATED_NAME)
    @ApiOperation(value = "Download avatar by FileName")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successful"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found", response = ErrorResponse.class),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorResponse.class)})
    public ResponseEntity<Resource> download(@PathVariable("docGeneratedName") String docGeneratedName, HttpServletRequest request) throws DataNotFoundException, InvalidDataException, MalformedURLException {

        Resource resource = fileDocumentsService.getDocumentAsResourceByGeneratedName(docGeneratedName);

        String contentType = null;

        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            ex.printStackTrace();
            //throw new InvalidDataException("Invalid Data");
        }

        if (contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }

    @GetMapping(value = GET_FILE_INFO_BY_GENERATED_NAME)
    @ApiOperation(value = "Get Info by FileName")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successful"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found", response = ErrorResponse.class),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorResponse.class)})
    public ResponseEntity<FileDocumentsResponse> getFileInfoByGeneratedName(@PathVariable("docGeneratedName") String docGeneratedName, HttpServletRequest request) throws DataNotFoundException, InvalidDataException, MalformedURLException {

        return ResponseEntity.ok(
                fileDocumentsConverter.convertFromEntity(
                        fileDocumentsService.getByGeneratedName(docGeneratedName)
                )
        );
    }

}
