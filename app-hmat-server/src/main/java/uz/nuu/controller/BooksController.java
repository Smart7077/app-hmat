package uz.nuu.controller;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.nuu.converter.BooksConverter;
import uz.nuu.exception.ApiException;
import uz.nuu.exception.DataAlreadyExistsException;
import uz.nuu.exception.DataNotFoundException;
import uz.nuu.payload.BooksRequest;
import uz.nuu.service.BooksTypeService;
import uz.nuu.service.impl.BooksServiceImpl;

import java.util.UUID;

@RestController
@RequestMapping("/books")
public class BooksController {
    private static final String CREATE = "/create";
    private static final String GET_BY_ID = "/{id}";
    private static final String GET_ALL = "";
    private static final String UPDATE = "/update/{id}";
    private static final String DELETE = "/{id}";

    private final BooksServiceImpl booksService;
    private final BooksTypeService booksTypeService;
    private final BooksConverter booksConverter;

    public BooksController(BooksServiceImpl booksService, BooksTypeService booksTypeService) {
        this.booksService = booksService;
        this.booksTypeService = booksTypeService;
        this.booksConverter = new BooksConverter();
    }

    private static final Logger logger = LogManager.getLogger("ControllerLogger");

    @PostMapping(CREATE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully created books"),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorController.class)})
    public ResponseEntity create(@RequestBody BooksRequest request) throws ApiException {
        logger.info("Create new Books : " + request);


        return ResponseEntity.ok(
                booksConverter.convertFromEntity(
                        booksService.create(booksConverter.convertFromRequest(request, booksTypeService.getById(request.getTypeId()))
                        )
                ));
    }

    @GetMapping(GET_BY_ID)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully getById books"),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorController.class)})
    public ResponseEntity getById(@PathVariable UUID id) throws ApiException {
        logger.info("Get Books by this id : " + id);
        return ResponseEntity.ok(
                booksConverter.convertFromEntity(
                        booksService.getById(id)
                )
        );
    }

    @GetMapping(GET_ALL)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully get all Books"),
            @ApiResponse(code = 500, message = "Something went error", response = ErrorController.class)})
    public ResponseEntity getAll() throws ApiException {
        logger.info("Get all Books : ");
        return ResponseEntity.ok(
                booksConverter.convertFromEntitiesList(
                        booksService.list()
                )
        );
    }

    @PutMapping(UPDATE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully books update"),
            @ApiResponse(code = 500, message = "Something went error", response = ErrorController.class)})
    public ResponseEntity update(@RequestBody BooksRequest request, @PathVariable UUID id) throws DataNotFoundException, DataAlreadyExistsException {
        logger.info("Update Books by this request : " + request + "id = " + id);

        return ResponseEntity.ok(
                booksConverter.convertFromEntity(
                        booksService.update(id, request, booksTypeService.getById(request.getTypeId()))
                ));
    }

    @DeleteMapping(DELETE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully delete books"),
            @ApiResponse(code = 500, message = "Something went error", response = ErrorController.class)})
    public ResponseEntity delete(@PathVariable UUID id) throws DataNotFoundException, DataAlreadyExistsException {
        logger.info("Delete books by this id : " + id);
        return ResponseEntity.ok(booksTypeService.delete(id));
    }
}
