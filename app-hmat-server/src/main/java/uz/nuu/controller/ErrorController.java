package uz.nuu.controller;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import uz.nuu.exception.CustomDataIntegrityViolationException;
import uz.nuu.exception.DataAlreadyExistsException;
import uz.nuu.exception.DataNotFoundException;
import uz.nuu.payload.ErrorResponse;

@RestControllerAdvice
public class ErrorController {

    @ExceptionHandler({DataNotFoundException.class})
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Data Not Found", response = DataNotFoundException.class)
    })
    public ResponseEntity<ErrorResponse> dataNotFoundException(Exception e, ServletWebRequest webRequest) {
        return new ResponseEntity<>(
                new ErrorResponse(
                        HttpStatus.NOT_FOUND.value(),
                        e.getClass().getSimpleName(),
                        webRequest.getRequest().getRequestURI(),
                        e.getLocalizedMessage()),
                HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({DataAlreadyExistsException.class})
    @ApiResponses(value = {
            @ApiResponse(code = 409, message = "Data already exists", response = DataAlreadyExistsException.class)
    })
    public ResponseEntity<ErrorResponse> dataAlreadyExists(Exception e, ServletWebRequest webRequest) {
        return new ResponseEntity<>(
                new ErrorResponse(
                        HttpStatus.CONFLICT.value(),
                        e.getClass().getSimpleName(),
                        webRequest.getRequest().getRequestURI(),
                        e.getLocalizedMessage()),
                HttpStatus.CONFLICT);
    }

    @ExceptionHandler({CustomDataIntegrityViolationException.class})
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Data invalid", response = CustomDataIntegrityViolationException.class)
    })
    public ResponseEntity<ErrorResponse> dataInvalid(Exception e, ServletWebRequest webRequest) {
        return new ResponseEntity<>(
                new ErrorResponse(
                        HttpStatus.BAD_REQUEST.value(),
                        e.getClass().getSimpleName(),
                        webRequest.getRequest().getRequestURI(),
                        e.getLocalizedMessage()),
                HttpStatus.BAD_REQUEST);
    }
}
