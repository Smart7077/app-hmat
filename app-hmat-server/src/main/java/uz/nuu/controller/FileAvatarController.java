package uz.nuu.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import uz.nuu.converter.FileAvatarConverter;
import uz.nuu.exception.DataNotFoundException;
import uz.nuu.exception.FileUploadException;
import uz.nuu.exception.InvalidDataException;
import uz.nuu.payload.ErrorResponse;
import uz.nuu.payload.FileAvatarUploadResponse;
import uz.nuu.service.impl.FileAvatarServiceImpl;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.UUID;

@RestController
@RequestMapping("/avatar")
public class FileAvatarController {

    private static final Logger logger = LogManager.getLogger("ControllerLogger");

    private final FileAvatarServiceImpl fileService;

    private final FileAvatarConverter fileAvatarConverter;

    public FileAvatarController(FileAvatarServiceImpl fileAvatarService) {
        super();
        this.fileService = fileAvatarService;
        fileAvatarConverter = new FileAvatarConverter();
    }

    @PostMapping(value = "/upload", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Add new  avatar")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successful"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found", response = ErrorResponse.class),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorResponse.class)})
    public ResponseEntity<FileAvatarUploadResponse> upload(@RequestParam("file") MultipartFile multipartFile) throws FileUploadException, InvalidDataException {
        logger.info("Create new file : " + multipartFile.getName());
        return ResponseEntity.ok(fileAvatarConverter.convertFromEntity(fileService.avatarUpload(multipartFile)));
    }

    @GetMapping(value = "/download/{id}")
    @ApiOperation(value = "Download avatar by Id")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successful"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found", response = ErrorResponse.class),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorResponse.class)})
    public ResponseEntity<Resource> download(@PathVariable("id") UUID id, HttpServletRequest request) throws DataNotFoundException, InvalidDataException {
        logger.info("Download file : " + id);
        Resource resource = fileService.getAvatarImgAsResourceByID(id);

        String contentType = null;

        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        if (contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }

    @GetMapping(value = "/download/name/{filename}")
    @ApiOperation(value = "Download avatar by FileName")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successful"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found", response = ErrorResponse.class),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorResponse.class)})
    public ResponseEntity<Resource> download(@PathVariable("filename") String fileName, HttpServletRequest request) throws DataNotFoundException, InvalidDataException {
        logger.info("download" + fileName);
        Resource resource = fileService.getAvatarImgAsResourceByGeneratedName(fileName);

        String contentType = null;

        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        if (contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }
}
