package uz.nuu.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.nuu.entity.FileDocumentTypeForFileDocumentsEntity;
import uz.nuu.payload.ErrorResponse;
import uz.nuu.service.impl.FileDocumentTypeForFileDocumentsServiceImpl;

import java.util.List;

@RestController
@RequestMapping("/file/fileDocumentType")
public class FileDocumentTypeForFileDocumentsController {

    private static final Logger logger = LogManager.getLogger("ControllerLogger");

    private FileDocumentTypeForFileDocumentsServiceImpl service;

    public FileDocumentTypeForFileDocumentsController() {
    }

    @GetMapping(value = "/all")
    @ApiOperation(value = "all document types")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successful"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found", response = ErrorResponse.class),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorResponse.class)})
    public ResponseEntity<List<FileDocumentTypeForFileDocumentsEntity>> download() {
        return ResponseEntity.ok(
                service.getAll());
    }
}
