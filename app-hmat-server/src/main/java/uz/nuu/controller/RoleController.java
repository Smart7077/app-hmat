package uz.nuu.controller;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.nuu.converter.PermissionConverter;
import uz.nuu.converter.RoleConverter;
import uz.nuu.exception.ApiException;
import uz.nuu.exception.DataAlreadyExistsException;
import uz.nuu.exception.DataNotFoundException;
import uz.nuu.payload.RoleRequest;
import uz.nuu.service.PermissionService;
import uz.nuu.service.RoleService;

@RestController
@RequestMapping("/v1/role")
public class RoleController {
    private static final String CREATE = "/create";
    private static final String GET_BY_ID = "/{id}";
    private static final String GET_ALL_PAGEABLE = "/pageable";
    private static final String GET_ALL = "";
    private static final String UPDATE = "/update/{id}";
    private static final String DELETE = "/{id}";

    private final RoleService roleService;
    private final PermissionService permissionService;
    private final RoleConverter roleConverter;
    private final PermissionConverter permissionConverter;

    public RoleController(RoleService roleService, PermissionService permissionService) {
        this.roleService = roleService;
        this.permissionService = permissionService;
        this.roleConverter = new RoleConverter();
        this.permissionConverter = new PermissionConverter();
    }

    private static final Logger logger = LogManager.getLogger("ControllerLogger");

    @PostMapping(CREATE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully created permission"),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorController.class)})
    public ResponseEntity create(@RequestBody RoleRequest request) throws ApiException {
        logger.info("Create new Role : " + request);
        return ResponseEntity.ok(
                roleConverter.convertFromEntity(
                        roleService.create(roleConverter.convertFromRequest(request, permissionService.findByIds(request.getPermissionIds())))
                )
        );
    }

    @GetMapping(GET_BY_ID)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully getById role"),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorController.class)})
    public ResponseEntity getById(@PathVariable Long id) throws ApiException {
        logger.info("Get Role by this id : " + id);
        return ResponseEntity.ok(
                roleConverter.convertFromEntity(
                        roleService.getById(id)
                )
        );
    }

    @GetMapping(GET_ALL_PAGEABLE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully get all role with pageable"),
            @ApiResponse(code = 500, message = "Something went error", response = ErrorController.class)})
    public ResponseEntity getAllPageable(@RequestParam int page, @RequestParam int size) throws ApiException {
        logger.info("Get all roles by this page : " + page + ", size " + size);
        return ResponseEntity.ok(
                roleConverter.convertFromRolePage(
                        roleService.list(PageRequest.of(page, size))
                )
        );
    }

    @GetMapping(GET_ALL)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully get all role"),
            @ApiResponse(code = 500, message = "Something went error", response = ErrorController.class)})
    public ResponseEntity getAll() throws ApiException {
        logger.info("Get all roles : ");
        return ResponseEntity.ok(
                roleConverter.convertFromRoleList(
                        roleService.list()
                )
        );
    }

    @PutMapping(UPDATE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully role update"),
            @ApiResponse(code = 500, message = "Something went error", response = ErrorController.class)})
    public ResponseEntity update(@RequestBody RoleRequest request, @PathVariable Long id) throws DataNotFoundException, DataAlreadyExistsException {
        logger.info("Update role by this request : " + request + "id = " + id);
        return ResponseEntity.ok(
                roleConverter.convertFromEntity(
                        roleService.update(id, roleConverter.convertFromRequest(request, permissionService.findByIds(request.getPermissionIds())))
                ));
    }

    @DeleteMapping(DELETE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully delete role"),
            @ApiResponse(code = 500, message = "Something went error", response = ErrorController.class)})
    public ResponseEntity delete(@PathVariable Long id) throws DataNotFoundException, DataAlreadyExistsException {
        logger.info("Delete role by this id : " + id);
        return ResponseEntity.ok(roleService.delete(id));
    }
}
