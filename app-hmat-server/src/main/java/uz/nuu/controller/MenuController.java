package uz.nuu.controller;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.nuu.converter.MenuConverter;
import uz.nuu.entity.Menu;
import uz.nuu.exception.ApiException;
import uz.nuu.exception.DataAlreadyExistsException;
import uz.nuu.exception.DataNotFoundException;
import uz.nuu.payload.MenuRequest;
import uz.nuu.service.MenuService;

@RestController
@RequestMapping("/menu")
public class MenuController {
    private static final String CREATE = "/create";
    private static final String GET_BY_ID = "/{id}";
    private static final String GET_ALL = "";
    private static final String UPDATE = "/update/{id}";
    private static final String DELETE = "/{id}";

    private final MenuService menuService;
    private final MenuConverter menuConverter;

    public MenuController(MenuService menuService   ) {
        this.menuService = menuService;
        this.menuConverter = new MenuConverter();
    }

    private static final Logger logger = LogManager.getLogger("ControllerLogger");

    @PostMapping(CREATE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully created permission"),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorController.class)})
    public ResponseEntity create(@RequestBody MenuRequest request) throws ApiException {
        logger.info("Create new Menu : " + request);
        Menu parentMenu = null;
        if (request.getParentId() != 0) {
            parentMenu = menuService.findById(request.getParentId());
        }
        return ResponseEntity.ok(
                menuConverter.convertFromEntity(
                        menuService.create(menuConverter.convertFromRequest(request, parentMenu)
                        )
                ));
    }

    @GetMapping(GET_BY_ID)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully getById role"),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorController.class)})
    public ResponseEntity getById(@PathVariable Long id) throws ApiException {
        logger.info("Get Menu by this id : " + id);
        return ResponseEntity.ok(
                menuConverter.convertFromEntity(
                        menuService.getById(id)
                )
        );
    }

    @GetMapping(GET_ALL)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully get all role"),
            @ApiResponse(code = 500, message = "Something went error", response = ErrorController.class)})
    public ResponseEntity getAll() throws ApiException {
        logger.info("Get all Menus : ");
        return ResponseEntity.ok(
                menuConverter.convertFromEntitiesList(
                        menuService.list()
                )
        );
    }

    @PutMapping(UPDATE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully role update"),
            @ApiResponse(code = 500, message = "Something went error", response = ErrorController.class)})
    public ResponseEntity update(@RequestBody MenuRequest request, @PathVariable Long id) throws DataNotFoundException, DataAlreadyExistsException {
        logger.info("Update menu by this request : " + request + "id = " + id);
        Menu parentMenu = null;
        if (request.getParentId() != 0) {
            parentMenu = menuService.findById(request.getParentId());
        }
        return ResponseEntity.ok(
                menuConverter.convertFromEntity(
                        menuService.update(id, menuConverter.convertFromRequest(request, parentMenu))
                ));
    }

    @DeleteMapping(DELETE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully delete role"),
            @ApiResponse(code = 500, message = "Something went error", response = ErrorController.class)})
    public ResponseEntity delete(@PathVariable Long id) throws DataNotFoundException, DataAlreadyExistsException {
        logger.info("Delete role by this id : " + id);
        return ResponseEntity.ok(menuService.delete(id));
    }
}
