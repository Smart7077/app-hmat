package uz.nuu.controller;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.nuu.converter.CategoryConverter;
import uz.nuu.exception.ApiException;
import uz.nuu.exception.DataAlreadyExistsException;
import uz.nuu.exception.DataNotFoundException;
import uz.nuu.payload.CategoryRequest;
import uz.nuu.payload.CategoryResponse;
import uz.nuu.service.CategoryService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/category")
public class CategoryController {
    private static final String CREATE = "/create";
    private static final String GET_BY_ID = "/{id}";
    private static final String GET_ALL = "";
    private static final String UPDATE = "/update/{id}";
    private static final String DELETE = "/{id}";

    private final CategoryService categoryService;
    private final CategoryConverter categoryConverter;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
        this.categoryConverter = new CategoryConverter();
    }

    private static final Logger logger = LogManager.getLogger("ControllerLogger");

    @PostMapping(CREATE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully created category"),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorController.class)})
    public ResponseEntity<CategoryResponse> create(@RequestBody CategoryRequest request) throws ApiException {
        logger.info("Create new category : " + request);
        return ResponseEntity.ok(
                categoryConverter.convertFromEntity(
                        categoryService.create(categoryConverter.convertFromRequest(request)
                        )
                ));
    }

    @GetMapping(GET_BY_ID)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully getById category"),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorController.class)})
    public ResponseEntity<CategoryResponse> getById(@PathVariable UUID id) throws ApiException {
        logger.info("Get category by this id : " + id);
        return ResponseEntity.ok(
                categoryConverter.convertFromEntity(
                        categoryService.getById(id)
                )
        );
    }

    @GetMapping(GET_ALL)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully get all category"),
            @ApiResponse(code = 500, message = "Something went error", response = ErrorController.class)})
    public ResponseEntity<List<CategoryResponse>> getAll() throws ApiException {
        logger.info("Get all category : ");
        return ResponseEntity.ok(
                categoryConverter.convertFromEntitiesList(
                        categoryService.list()
                )
        );
    }

    @PutMapping(UPDATE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully role update"),
            @ApiResponse(code = 500, message = "Something went error", response = ErrorController.class)})
    public ResponseEntity<CategoryResponse> update(@RequestBody CategoryRequest request, @PathVariable UUID id) throws DataNotFoundException, DataAlreadyExistsException {
        logger.info("Update menu by this request : " + request + "id = " + id);
        return ResponseEntity.ok(
                categoryConverter.convertFromEntity(
                        categoryService.update(request, id)
                ));
    }

    @DeleteMapping(DELETE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully delete role"),
            @ApiResponse(code = 500, message = "Something went error", response = ErrorController.class)})
    public ResponseEntity<Boolean> delete(@PathVariable UUID id) throws DataNotFoundException, DataAlreadyExistsException {
        logger.info("Delete role by this id : " + id);
        return ResponseEntity.ok(categoryService.delete(id));
    }

}

