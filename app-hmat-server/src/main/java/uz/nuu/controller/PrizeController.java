package uz.nuu.controller;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.nuu.converter.PrizeConverter;
import uz.nuu.entity.Students;
import uz.nuu.exception.ApiException;
import uz.nuu.exception.DataAlreadyExistsException;
import uz.nuu.exception.DataNotFoundException;
import uz.nuu.payload.PrizesRequest;
import uz.nuu.payload.PrizesResponse;
import uz.nuu.service.impl.PrizeServiceImpl;
import uz.nuu.service.impl.StudentsServiceImpl;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/prize")
public class PrizeController {
    private static final String CREATE = "/create";
    private static final String GET_BY_ID = "/{id}";
    private static final String GET_ALL = "";
    private static final String UPDATE = "/update/{id}";
    private static final String DELETE = "/{id}";

    private final PrizeServiceImpl prizeService;
    private final StudentsServiceImpl studentsService;
    private final PrizeConverter prizeConverter;

    public PrizeController(PrizeServiceImpl prizeService, StudentsServiceImpl studentsService) {
        this.prizeService = prizeService;
        this.studentsService = studentsService;
        this.prizeConverter = new PrizeConverter();
    }

    private static final Logger logger = LogManager.getLogger("ControllerLogger");

    @PostMapping(CREATE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully created prize"),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorController.class)})
    public ResponseEntity create(@RequestBody PrizesRequest request) throws ApiException {
        logger.info("Create new prize : " + request);

        Students students = null;
        if (request.getStudentId() != null) {
            students = studentsService.getById(request.getStudentId());
        }

        return ResponseEntity.ok(
                prizeConverter.convertFromEntity(
                        prizeService.create(prizeConverter.convertFromRequest(request, students))
                ));
    }

    @GetMapping(GET_BY_ID)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully getById prize"),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorController.class)})
    public ResponseEntity getById(@PathVariable UUID id) throws ApiException {
        logger.info("Get prize by this id : " + id);
        return ResponseEntity.ok(
                prizeConverter.convertFromEntity(prizeService.getById(id))
        );
    }

    @GetMapping(GET_ALL)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully get all Prize"),
            @ApiResponse(code = 500, message = "Something went error", response = ErrorController.class)})
    public ResponseEntity<List<PrizesResponse>> getAll() throws ApiException {
        logger.info("Get all Prizes : ");
        return ResponseEntity.ok(
                prizeConverter.convertFromEntitiesList(
                        prizeService.list()
                )
        );
    }

    @PutMapping(UPDATE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully prizes update"),
            @ApiResponse(code = 500, message = "Something went error", response = ErrorController.class)})
    public ResponseEntity update(@RequestBody PrizesRequest request, @PathVariable UUID id) throws DataNotFoundException, DataAlreadyExistsException {
        logger.info("Update prizes by this request : " + request + "id = " + id);

        return ResponseEntity.ok(
                prizeConverter.convertFromEntity(
                        prizeService.update(id, request, studentsService.getById(request.getStudentId()))
                ));
    }

    @DeleteMapping(DELETE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully delete prize"),
            @ApiResponse(code = 500, message = "Something went error", response = ErrorController.class)})
    public ResponseEntity delete(@PathVariable UUID id) throws DataNotFoundException, DataAlreadyExistsException {
        logger.info("Delete prize by this id : " + id);
        return ResponseEntity.ok(prizeService.delete(id));
    }
}
