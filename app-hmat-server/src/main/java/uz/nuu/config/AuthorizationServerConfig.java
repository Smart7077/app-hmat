package uz.nuu.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.AccessTokenConverter;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;

@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {
    private String clientId = "clientId";
    private String clientSecret = "clientSecret";
    private String grantType = "password";
    private String grantTypeRefresh = "refresh_token";
    private String scopeRead = "read";
    private String scopeWrite = "write";
    private String scopeUpdate = "update";
    private String scopeDelete = "delete";
    private String resourceIds = "ResourceId";

    private final AuthenticationManager authenticationManager;
    private final DefaultTokenServices defaultTokenServices;
    private final UserDetailsService userDetailsService;
    private final AccessTokenConverter accessTokenConverter;
    private final BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder(10);

    public AuthorizationServerConfig(AuthenticationManager authenticationManager, DefaultTokenServices defaultTokenServices, UserDetailsService userDetailsService, AccessTokenConverter accessTokenConverter) {
        this.authenticationManager = authenticationManager;
        this.defaultTokenServices = defaultTokenServices;
        this.userDetailsService = userDetailsService;
        this.accessTokenConverter = accessTokenConverter;
    }

    @Override
    public void configure(ClientDetailsServiceConfigurer configurer) throws Exception {
        configurer
                .inMemory()
                .withClient(clientId)
                .secret(passwordEncoder.encode(clientSecret))
                .authorizedGrantTypes(grantType, grantTypeRefresh)
                .scopes(scopeRead, scopeWrite, scopeUpdate, scopeDelete)
                .resourceIds(resourceIds);
    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        endpoints.tokenServices(defaultTokenServices).authenticationManager(authenticationManager)
                .accessTokenConverter(accessTokenConverter).userDetailsService(userDetailsService);
    }

    @Override
    public void configure(final AuthorizationServerSecurityConfigurer oauthServer) {
        oauthServer
                .passwordEncoder(this.passwordEncoder)
                .tokenKeyAccess("permitAll()")
                .checkTokenAccess("permitAll()");
    }
}