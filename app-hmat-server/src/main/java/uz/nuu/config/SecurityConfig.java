package uz.nuu.config;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.*;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private String signingKey = "MaYzkSjmkzPC57L";

    private String securityRealm = "EmblaMagazineRealm";

    @Autowired
    private PasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private UserDetailsService userDetailsService;

    @Bean
    @Override
    protected AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/login", "/oauth/token", "/oauth/check_token", "/user/create")
                .permitAll();
        final String[] AUTH_WHITELIST = {
                "/swagger-resources/**",
                "/swagger-ui.html",
                "/v2/api-docs",
                "/oauth/token/",
                "/oauth/authorize",
                "/oauth/check_token",
                "/webjars/**",

                "/books/create",
                "/books/{id}",
                "/books",
                "/books/update/{id}",
                "/books/{id}",

                "/booksType/create",
                "/booksType/{id}",
                "/booksType",
                "/booksType/update/{id}",
                "/booksType/{id}",

                "/category/create",
                "/category/{id}",
                "/category",
                "/category/update/{id}",
                "/category/{id}",

                "/feedback/create",
                "/feedback/{id}",
                "/feedback",
                "/feedback/update/{id}",
                "/feedback/{id}",

                "/menu/create",
                "/menu/{id}",
                "/menu",
                "/menu/update/{id}",
                "/menu/{id}",

                "/teachers/create",
                "/teachers/{id}",
                "/teachers",
                "/teachers/update/{id}",
                "/teachers/{id}",

                "/subjects/create",
                "/subjects/{id}",
                "/subjects",
                "/subjects/update/{id}",
                "/subjects/{id}",

                "/articles/create",
                "/articles/{id}",
                "/articles",
                "/articles/update/{id}",
                "/articles/{id}",

                "/avatar/upload",
                "/avatar/download/{id}",
                "/avatar/download/name/{filename}",

                "/students/create",
                "/students/{id}",
                "/students",
                "/students/update/{id}",
                "/students/{id}",
                "/file/fileDocumentType/all",

                "/file/documents/upload",
                "/file/documents/download/{documentId}",
                "/file/documents/download/name/{docGeneratedName}",
                "/file/documents/get/info/{docGeneratedName}",

                "/prize/create",
                "/prize/{id}",
                "/prize",
                "/prize/update/{id}",
                "/prize/{id}",

                "/word/create",
                "/word/{id}",
                "/word",
                "/word/update/{id}",
                "/word/{id}",

                "/news/create",
                "/news/{id}",
                "/news",
                "/news/pageable",
                "/news/update/{id}",
                "/news/{id}",
                "/news/by/category/pageable",

                "/user/{id}",
                "/user/create",
                "/user/current",
                "/user/pageable",
                "/user/update/{id}",
                "/user/{id}",
                "/user/roles/{username}"
        };

        http.authorizeRequests()
                .antMatchers(AUTH_WHITELIST)
                .permitAll()
                .anyRequest().authenticated();
        http
                .csrf()
                .disable();
    }

    @Bean
    public UserAuthenticationConverter userAuthenticationConverter() {
        DefaultUserAuthenticationConverter defaultUserAuthenticationConverter = new DefaultUserAuthenticationConverter();
        defaultUserAuthenticationConverter.setUserDetailsService(userDetailsService);
        return defaultUserAuthenticationConverter;
    }

    @Bean("jwtAccessTokenConverter")
    public JwtAccessTokenConverter jwtAccessTokenConverter() {
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        converter.setSigningKey(signingKey);
        converter.setVerifierKey(signingKey);
        ((DefaultAccessTokenConverter) converter.getAccessTokenConverter())
                .setUserTokenConverter(userAuthenticationConverter());
        return converter;
    }

    @Bean
    public TokenStore tokenStore() {
        return new JwtTokenStore(jwtAccessTokenConverter());
    }

    @Bean
    public TokenEnhancerChain tokenEnhancerChain() {
        final TokenEnhancerChain tokenEnhancerChain = new TokenEnhancerChain();
        tokenEnhancerChain.setTokenEnhancers(Lists.newArrayList(new MyTokenEnhancer(), jwtAccessTokenConverter()));
        return tokenEnhancerChain;
    }

    @Bean
    public DefaultTokenServices defaultTokenServices() {
        final DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
        defaultTokenServices.setTokenStore(tokenStore());
        // defaultTokenServices.setClientDetailsService(clientDetailsService);
        defaultTokenServices.setTokenEnhancer(tokenEnhancerChain());
        defaultTokenServices.setSupportRefreshToken(true);
        return defaultTokenServices;
    }

    private static class MyTokenEnhancer implements TokenEnhancer {
        @Override
        public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
            return accessToken;
        }
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(10);
    }
}