package uz.nuu.payload;

import java.util.UUID;

public class FileAvatarUploadResponse {

    private UUID id;
    private String originalName;
    private String generatedName;

    public FileAvatarUploadResponse(UUID id, String originalName, String generatedName) {
        this.id = id;
        this.originalName = originalName;
        this.generatedName = generatedName;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getOriginalName() {
        return originalName;
    }

    public void setOriginalName(String originalName) {
        this.originalName = originalName;
    }

    public String getGeneratedName() {
        return generatedName;
    }

    public void setGeneratedName(String generatedName) {
        this.generatedName = generatedName;
    }

    @Override
    public String toString() {
        return "FileAvatarUploadResponse{" +
                "id=" + id +
                ", originalName='" + originalName + '\'' +
                ", generatedName='" + generatedName + '\'' +
                '}';
    }
}
