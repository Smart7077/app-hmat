package uz.nuu.payload;

import java.util.HashMap;
import java.util.UUID;

public class ProductionProcessRequest {
    private HashMap<String, String> cyName;
    private HashMap<String, String> cyAddress;
    private String cyPhoneNumber;
    private String startDate;
    private String endDate;
    private String rating;
    private String documentationLink;
    private HashMap<String, String> title;
    private UUID studentId;
    private UUID teacherId;

    public ProductionProcessRequest() {
    }

    public ProductionProcessRequest(HashMap<String, String> cyName, HashMap<String, String> cyAddress, String cyPhoneNumber, String startDate, String endDate, String rating, String documentationLink, HashMap<String, String> title, UUID studentId, UUID teacherId) {
        this.cyName = cyName;
        this.cyAddress = cyAddress;
        this.cyPhoneNumber = cyPhoneNumber;
        this.startDate = startDate;
        this.endDate = endDate;
        this.rating = rating;
        this.documentationLink = documentationLink;
        this.title = title;
        this.studentId = studentId;
        this.teacherId = teacherId;
    }

    public HashMap<String, String> getCyName() {
        return cyName;
    }

    public void setCyName(HashMap<String, String> cyName) {
        this.cyName = cyName;
    }

    public HashMap<String, String> getCyAddress() {
        return cyAddress;
    }

    public void setCyAddress(HashMap<String, String> cyAddress) {
        this.cyAddress = cyAddress;
    }

    public String getCyPhoneNumber() {
        return cyPhoneNumber;
    }

    public void setCyPhoneNumber(String cyPhoneNumber) {
        this.cyPhoneNumber = cyPhoneNumber;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getDocumentationLink() {
        return documentationLink;
    }

    public void setDocumentationLink(String documentationLink) {
        this.documentationLink = documentationLink;
    }

    public HashMap<String, String> getTitle() {
        return title;
    }

    public void setTitle(HashMap<String, String> title) {
        this.title = title;
    }

    public UUID getStudentId() {
        return studentId;
    }

    public void setStudentId(UUID studentId) {
        this.studentId = studentId;
    }

    public UUID getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(UUID teacherId) {
        this.teacherId = teacherId;
    }

    @Override
    public String toString() {
        return "ProductionProcessRequest{" +
                ", cyName=" + cyName +
                ", cyAddress=" + cyAddress +
                ", cyPhoneNumber='" + cyPhoneNumber + '\'' +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", rating='" + rating + '\'' +
                ", documentationLink='" + documentationLink + '\'' +
                ", title=" + title +
                ", studentId=" + studentId +
                ", teacherId=" + teacherId +
                '}';
    }
}
