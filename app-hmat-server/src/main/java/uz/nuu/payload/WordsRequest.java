package uz.nuu.payload;

import java.util.HashMap;

public class WordsRequest {
    private HashMap<String, String> name;

    public WordsRequest(HashMap<String, String> name) {
        this.name = name;
    }

    public HashMap<String, String> getName() {
        return name;
    }

    public void setName(HashMap<String, String> name) {
        this.name = name;
    }
}
