package uz.nuu.payload;

import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

public class PrizesRequest {

    private HashMap<String, String> name;
    private Date givenTime;
    private UUID studentId;
    private String givenBy;

    public PrizesRequest() {
    }

    public PrizesRequest( HashMap<String, String> name, Date givenTime, UUID studentId, String givenBy) {
        this.name = name;
        this.givenTime = givenTime;
        this.studentId = studentId;
        this.givenBy = givenBy;
    }

    public HashMap<String, String> getName() {
        return name;
    }

    public void setName(HashMap<String, String> name) {
        this.name = name;
    }

    public Date getGivenTime() {
        return givenTime;
    }

    public void setGivenTime(Date givenTime) {
        this.givenTime = givenTime;
    }

    public UUID getStudentId() {
        return studentId;
    }

    public void setStudentId(UUID studentId) {
        this.studentId = studentId;
    }

    public String getGivenBy() {
        return givenBy;
    }

    public void setGivenBy(String givenBy) {
        this.givenBy = givenBy;
    }
}
