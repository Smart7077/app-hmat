package uz.nuu.payload;

import java.util.HashMap;
import java.util.UUID;

public class CategoryResponse {
    private UUID id;
    private HashMap<String, String> name;

    public CategoryResponse(UUID id, HashMap<String, String> name) {
        this.id = id;
        this.name = name;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public HashMap<String, String> getName() {
        return name;
    }

    public void setName(HashMap<String, String> name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "CategoryResponse{" +
                "id=" + id +
                ", name=" + name +
                '}';
    }
}
