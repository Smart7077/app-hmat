package uz.nuu.payload;

import javax.validation.constraints.NotNull;

public class PermissionRequest {
    @NotNull
    private String name;
    @NotNull
    private String title;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
