package uz.nuu.payload;

import java.util.List;

public class RoleResponse {
    private Long id;
    private String name;
    private String systemName;
    private String description;
    private boolean active;
    private List<PermissionResponse> permissionResponseList;

    public RoleResponse() {
    }

    public RoleResponse(Long id, String name, String systemName, String description, boolean active, List<PermissionResponse> permissionResponseList) {
        this.id = id;
        this.name = name;
        this.systemName = systemName;
        this.description = description;
        this.active = active;
        this.permissionResponseList = permissionResponseList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSystemName() {
        return systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public List<PermissionResponse> getPermissionResponseList() {
        return permissionResponseList;
    }

    public void setPermissionResponseList(List<PermissionResponse> permissionResponseList) {
        this.permissionResponseList = permissionResponseList;
    }

    @Override
    public String toString() {
        return "RoleResponse{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", systemName='" + systemName + '\'' +
                ", description='" + description + '\'' +
                ", active=" + active +
                ", permissionResponseList=" + permissionResponseList +
                '}';
    }
}
