package uz.nuu.payload;

import uz.nuu.entity.Teachers;

import java.util.*;

public class ArticlesResponse {
    private UUID id;
    private HashMap<String, String> title;
    private HashMap<String, String> journalName;
    private Long pageCount;
    private Date publishedDate;
    private String bookUrl;
    private String type;
    private String mFacter;
    private Collection<Teachers> teachersIdList;

    public ArticlesResponse() {
    }

    public ArticlesResponse(UUID id, HashMap<String, String> title, HashMap<String, String> journalName, Long pageCount, Date publishedDate, String bookUrl, String type, String mFacter, Collection<Teachers> teachersIdList) {
        this.id = id;
        this.title = title;
        this.journalName = journalName;
        this.pageCount = pageCount;
        this.publishedDate = publishedDate;
        this.bookUrl = bookUrl;
        this.type = type;
        this.mFacter = mFacter;
        this.teachersIdList = teachersIdList;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public HashMap<String, String> getTitle() {
        return title;
    }

    public void setTitle(HashMap<String, String> title) {
        this.title = title;
    }

    public HashMap<String, String> getJournalName() {
        return journalName;
    }

    public void setJournalName(HashMap<String, String> journalName) {
        this.journalName = journalName;
    }

    public Long getPageCount() {
        return pageCount;
    }

    public void setPageCount(Long pageCount) {
        this.pageCount = pageCount;
    }

    public Date getPublishedDate() {
        return publishedDate;
    }

    public void setPublishedDate(Date publishedDate) {
        this.publishedDate = publishedDate;
    }

    public String getBookUrl() {
        return bookUrl;
    }

    public void setBookUrl(String bookUrl) {
        this.bookUrl = bookUrl;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getmFacter() {
        return mFacter;
    }

    public void setmFacter(String mFacter) {
        this.mFacter = mFacter;
    }

    public Collection<Teachers> getTeachersIdList() {
        return teachersIdList;
    }

    public void setTeachersIdList(Collection<Teachers> teachersIdList) {
        this.teachersIdList = teachersIdList;
    }

    @Override
    public String toString() {
        return "ArticlesResponse{" +
                "id=" + id +
                ", title=" + title +
                ", journalName=" + journalName +
                ", pageCount=" + pageCount +
                ", publishedDate=" + publishedDate +
                ", bookUrl='" + bookUrl + '\'' +
                ", type='" + type + '\'' +
                ", mFacter='" + mFacter + '\'' +
                ", teachersIdList=" + teachersIdList +
                '}';
    }
}