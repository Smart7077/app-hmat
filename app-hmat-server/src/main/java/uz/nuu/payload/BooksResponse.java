package uz.nuu.payload;

import uz.nuu.entity.BooksType;

import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

public class BooksResponse {
    private UUID id;
    private HashMap<String, String> name;
    private Long pageCount;
    private HashMap<String, String> publishAddress;
    private Date publishedDate;
    private HashMap<String, String> authors;
    private String link;
    private BooksType booksType;
    private UUID typeId;
    private String internetLink;
    private Date createdDate;
    private Date modifiedDate;
    private String createdBy;
    private String modifiedBy;
    private Boolean status;
    private String img;

    public BooksResponse() {
    }

    public BooksResponse(UUID id, HashMap<String, String> name, Long pageCount, HashMap<String, String> publishAddress, Date publishedDate, HashMap<String, String> authors, String link, BooksType booksType, UUID typeId, String internetLink, Date createdDate, Date modifiedDate, String createdBy, String modifiedBy, Boolean status, String img) {
        this.id = id;
        this.name = name;
        this.pageCount = pageCount;
        this.publishAddress = publishAddress;
        this.publishedDate = publishedDate;
        this.authors = authors;
        this.link = link;
        this.booksType = booksType;
        this.typeId = typeId;
        this.internetLink = internetLink;
        this.createdDate = createdDate;
        this.modifiedDate = modifiedDate;
        this.createdBy = createdBy;
        this.modifiedBy = modifiedBy;
        this.status = status;
        this.img = img;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public HashMap<String, String> getName() {
        return name;
    }

    public void setName(HashMap<String, String> name) {
        this.name = name;
    }

    public Long getPageCount() {
        return pageCount;
    }

    public void setPageCount(Long pageCount) {
        this.pageCount = pageCount;
    }

    public HashMap<String, String> getPublishAddress() {
        return publishAddress;
    }

    public void setPublishAddress(HashMap<String, String> publishAddress) {
        this.publishAddress = publishAddress;
    }

    public Date getPublishedDate() {
        return publishedDate;
    }

    public void setPublishedDate(Date publishedDate) {
        this.publishedDate = publishedDate;
    }

    public HashMap<String, String> getAuthors() {
        return authors;
    }

    public void setAuthors(HashMap<String, String> authors) {
        this.authors = authors;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public BooksType getBooksType() {
        return booksType;
    }

    public void setBooksType(BooksType booksType) {
        this.booksType = booksType;
    }

    public UUID getTypeId() {
        return typeId;
    }

    public void setTypeId(UUID typeId) {
        this.typeId = typeId;
    }

    public String getInternetLink() {
        return internetLink;
    }

    public void setInternetLink(String internetLink) {
        this.internetLink = internetLink;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "BooksResponse{" +
                "id=" + id +
                ", name=" + name +
                ", pageCount=" + pageCount +
                ", publishAddress=" + publishAddress +
                ", publishedDate=" + publishedDate +
                ", authors=" + authors +
                ", link='" + link + '\'' +
                ", booksType=" + booksType +
                ", typeId=" + typeId +
                ", internetLink='" + internetLink + '\'' +
                ", createdDate=" + createdDate +
                ", modifiedDate=" + modifiedDate +
                ", createdBy='" + createdBy + '\'' +
                ", modifiedBy='" + modifiedBy + '\'' +
                ", status=" + status +
                '}';
    }
}
