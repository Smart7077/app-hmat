package uz.nuu.payload;

import java.util.HashMap;

public class StudentRequest {
    private HashMap<String, String> firstName;
    private HashMap<String, String> lastName;
    private HashMap<String, String> middleName;
    private HashMap<String, String> address;
    private HashMap<String, String> direction;
    private Long course;
    private String urlImage;
    private HashMap<String, String> readingType;
    private HashMap<String, String> degreeType;
    private String email;
    private String facebook;
    private String linkedIn;
    private String twitter;
    private String instagram;

    public StudentRequest() {
    }

    public StudentRequest(HashMap<String, String> firstName, HashMap<String, String> lastName, HashMap<String, String> middleName, HashMap<String, String> address, HashMap<String, String> direction, Long course, String urlImage, HashMap<String, String> readingType, HashMap<String, String> degreeType, String email, String facebook, String linkedIn, String twitter, String instagram) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.address = address;
        this.direction = direction;
        this.course = course;
        this.urlImage = urlImage;
        this.readingType = readingType;
        this.degreeType = degreeType;
        this.email = email;
        this.facebook = facebook;
        this.linkedIn = linkedIn;
        this.twitter = twitter;
        this.instagram = instagram;
    }

    public HashMap<String, String> getFirstName() {
        return firstName;
    }

    public void setFirstName(HashMap<String, String> firstName) {
        this.firstName = firstName;
    }

    public HashMap<String, String> getLastName() {
        return lastName;
    }

    public void setLastName(HashMap<String, String> lastName) {
        this.lastName = lastName;
    }

    public HashMap<String, String> getMiddleName() {
        return middleName;
    }

    public void setMiddleName(HashMap<String, String> middleName) {
        this.middleName = middleName;
    }

    public HashMap<String, String> getAddress() {
        return address;
    }

    public void setAddress(HashMap<String, String> address) {
        this.address = address;
    }

    public HashMap<String, String> getDirection() {
        return direction;
    }

    public void setDirection(HashMap<String, String> direction) {
        this.direction = direction;
    }

    public Long getCourse() {
        return course;
    }

    public void setCourse(Long course) {
        this.course = course;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public HashMap<String, String> getReadingType() {
        return readingType;
    }

    public void setReadingType(HashMap<String, String> readingType) {
        this.readingType = readingType;
    }

    public HashMap<String, String> getDegreeType() {
        return degreeType;
    }

    public void setDegreeType(HashMap<String, String> degreeType) {
        this.degreeType = degreeType;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getLinkedIn() {
        return linkedIn;
    }

    public void setLinkedIn(String linkedIn) {
        this.linkedIn = linkedIn;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }
}
