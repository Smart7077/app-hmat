package uz.nuu.payload;

public class FeedbackRequest {
    private String fio;
    private String email;
    private String subject;
    private String content;

    public FeedbackRequest(String fio, String email, String subject, String content) {
        this.fio = fio;
        this.email = email;
        this.subject = subject;
        this.content = content;
    }

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
