package uz.nuu.payload;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

public class ErrorResponse implements Serializable {
    private static final long serialVersionUID = -7026789325100666318L;
    private int status;
    private String error;
    private String path;
    private String message;
    private Date timestamp;
    private UUID errorIdForSend;

    public ErrorResponse() {
    }

    public ErrorResponse(int status, String error, String path, String message) {
        this.status = status;
        this.error = error;
        this.path = path;
        this.message = message;
        this.timestamp = new Date(System.currentTimeMillis());
    }

    public ErrorResponse(int status, String error, String path, String message, UUID errorIdForSend) {
        this.status = status;
        this.error = error;
        this.path = path;
        this.message = message;
        this.timestamp = new Date(System.currentTimeMillis());
        this.errorIdForSend = errorIdForSend;
    }

    public ErrorResponse(int status, String error, String path, String message, Date timestamp, UUID errorIdForSend) {
        this.status = status;
        this.error = error;
        this.path = path;
        this.message = message;
        this.timestamp = timestamp;
        this.errorIdForSend = errorIdForSend;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "ErrorResponse{" +
                "status=" + status +
                ", error='" + error +
                "', path='" + path +
                "', message='" + message +
                "', timestamp=" + timestamp +
                '}';
    }
}
