package uz.nuu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
public class Hmat {
    public static void main(String[] args) {
        SpringApplication.run(Hmat.class, args);
    }
}
