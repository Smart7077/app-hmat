package uz.nuu.converter;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import uz.nuu.entity.Permission;
import uz.nuu.payload.PermissionRequest;
import uz.nuu.payload.PermissionResponse;

import java.util.List;
import java.util.stream.Collectors;

public class PermissionConverter extends Converter<PermissionResponse, Permission> {
    public PermissionConverter() {
        super(fromDto -> new Permission(),
                fromEntity -> new PermissionResponse(
                        fromEntity.getId(),
                        fromEntity.getName(),
                        fromEntity.getTitle()
                )
        );
    }

    public Permission convertFromRequest(PermissionRequest request) {
        return new Permission(request.getName(), request.getTitle());
    }

    public Page<PermissionResponse> convertFromEntities(Page<Permission> permissions) {

        return new PageImpl<>(
                permissions.getContent()
                        .stream()
                        .map(permission -> new PermissionResponse(permission.getId(), permission.getName(), permission.getTitle()))
                        .collect(Collectors.toList()),permissions.getPageable(),permissions.getTotalElements()
        );
    }

    public List<PermissionResponse> convertFromEntitiesList(List<Permission> permissions) {

        return permissions.stream()
                .map(permission -> new PermissionResponse(permission.getId(), permission.getName(), permission.getTitle()))
                .collect(Collectors.toList());
    }
}
