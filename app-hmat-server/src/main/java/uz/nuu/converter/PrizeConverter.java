package uz.nuu.converter;

import uz.nuu.entity.Prizes;
import uz.nuu.entity.Students;
import uz.nuu.payload.PrizesRequest;
import uz.nuu.payload.PrizesResponse;

import java.util.List;
import java.util.stream.Collectors;

public class PrizeConverter extends Converter<PrizesResponse, Prizes> {
    public PrizeConverter() {
        super(fromDto -> new Prizes(),
                fromEntity -> new PrizesResponse(
                        fromEntity.getId(),
                        fromEntity.getName(),
                        fromEntity.getGivenTime(),
                        fromEntity.getStudents(),
                        fromEntity.getGivenBy()
                )
        );
    }

    public Prizes convertFromRequest(PrizesRequest request, Students students) {
        Prizes prizes = new Prizes(
                request.getName(),
                request.getGivenTime(),
                students,
                request.getStudentId(),
                request.getGivenBy()
        );
        return null;
    }

    public List<PrizesResponse> convertFromEntitiesList(List<Prizes> responseList) {
        return responseList
                .stream()
                .map(fromEntity -> new PrizesResponse(
                                fromEntity.getId(),
                                fromEntity.getName(),
                                fromEntity.getGivenTime(),
                                fromEntity.getStudents(),
                                fromEntity.getGivenBy()
                        )
                    )
                .collect(Collectors.toList());
    }
}
