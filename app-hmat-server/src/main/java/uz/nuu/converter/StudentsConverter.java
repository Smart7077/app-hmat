package uz.nuu.converter;

import uz.nuu.entity.Students;
import uz.nuu.payload.StudentRequest;
import uz.nuu.payload.StudentResponse;

import java.util.List;
import java.util.stream.Collectors;

public class StudentsConverter extends Converter<StudentResponse, Students> {
    public StudentsConverter() {
        super(fromDto -> new Students(),
                fromEntity -> new StudentResponse(
                        fromEntity.getId(),
                        fromEntity.getFirstName(),
                        fromEntity.getLastName(),
                        fromEntity.getMiddleName(),
                        fromEntity.getAddress(),
                        fromEntity.getDirection(),
                        fromEntity.getCourse(),
                        fromEntity.getUrlImage(),
                        fromEntity.getReadingType(),
                        fromEntity.getDegreeType(),
                        fromEntity.getEmail(),
                        fromEntity.getFacebook(),
                        fromEntity.getLinkedIn(),
                        fromEntity.getTwitter(),
                        fromEntity.getInstagram()
                )
        );
    }

    public Students convertFromRequest(StudentRequest request) {
        return new Students(
                request.getFirstName(),
                request.getLastName(),
                request.getMiddleName(),
                request.getAddress(),
                request.getDirection(),
                request.getCourse(),
                request.getUrlImage(),
                request.getReadingType(),
                request.getDegreeType(),
                request.getEmail(),
                request.getFacebook(),
                request.getLinkedIn(),
                request.getTwitter(),
                request.getInstagram()
        );
    }

    public List<StudentResponse> convertFromEntitiesList(List<Students> studentsList) {

        return studentsList.stream()
                .map(fromEntity -> new StudentResponse(
                                fromEntity.getId(),
                                fromEntity.getFirstName(),
                                fromEntity.getLastName(),
                                fromEntity.getMiddleName(),
                                fromEntity.getAddress(),
                                fromEntity.getDirection(),
                                fromEntity.getCourse(),
                                fromEntity.getUrlImage(),
                                fromEntity.getReadingType(),
                                fromEntity.getDegreeType(),
                                fromEntity.getEmail(),
                                fromEntity.getFacebook(),
                                fromEntity.getLinkedIn(),
                                fromEntity.getTwitter(),
                                fromEntity.getInstagram()
                        )
                )
                .collect(Collectors.toList());
    }
}
