package uz.nuu.converter;

import uz.nuu.entity.Menu;
import uz.nuu.payload.MenuRequest;
import uz.nuu.payload.MenuResponse;

import java.util.List;
import java.util.stream.Collectors;

public class MenuConverter extends Converter<MenuResponse, Menu> {
    public MenuConverter() {
        super(
                fromDto -> new Menu(),
                fromEntity -> new MenuResponse(fromEntity.getTitle(), fromEntity.getParentMenu(), fromEntity.getId())
        );
    }

    public Menu convertFromRequest(MenuRequest menuRequest, Menu menu) {
        return new Menu(menuRequest.getTitle(), menu);
    }

    public List<MenuResponse> convertFromEntitiesList(List<Menu> menus) {

        return menus.stream()
                .map(menu -> new MenuResponse(menu.getTitle(), menu.getParentMenu(), menu.getId()))
                .collect(Collectors.toList());
    }
}
