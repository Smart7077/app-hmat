package uz.nuu.converter;

import uz.nuu.entity.Feedback;
import uz.nuu.payload.FeedBackResponse;
import uz.nuu.payload.FeedbackRequest;

import java.util.List;
import java.util.stream.Collectors;

public class FeedBackConverter extends Converter<FeedBackResponse, Feedback> {
    public FeedBackConverter() {
        super(
                fromDto -> new Feedback(),
                fromEntity -> new FeedBackResponse(
                        fromEntity.getId(),
                        fromEntity.getFio(),
                        fromEntity.getEmail(),
                        fromEntity.getSubject(),
                        fromEntity.getContent()
                )
        );
    }

    public Feedback convertFromRequest(FeedbackRequest request) {
        return new Feedback(request.getFio(), request.getEmail(), request.getSubject(), request.getContent());
    }

    public List<FeedBackResponse> convertFromEntitiesList(List<Feedback> feedbackList) {

        return feedbackList.stream()
                .map(item -> new FeedBackResponse(item.getId(), item.getFio(), item.getEmail(), item.getSubject(), item.getContent()))
                .collect(Collectors.toList());
    }
}
