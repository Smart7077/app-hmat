package uz.nuu.converter;


import uz.nuu.entity.FileDocuments;
import uz.nuu.payload.FileDocumentsResponse;

public class FileDocumentsConverter extends Converter<FileDocumentsResponse, FileDocuments> {

    public FileDocumentsConverter() {
        super(fromDto -> new FileDocuments(),
                fromEntity -> {
                    FileDocumentsResponse fileDocumentsResponse = new FileDocumentsResponse(
                            fromEntity.getId(),
                            fromEntity.getDocumentType(),
                            fromEntity.getOriginalName(),
                            fromEntity.getGeneratedName()
                    );
                    return fileDocumentsResponse;
                });
    }
}
