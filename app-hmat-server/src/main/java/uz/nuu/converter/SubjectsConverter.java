package uz.nuu.converter;

import uz.nuu.entity.Subjects;
import uz.nuu.entity.Teachers;
import uz.nuu.payload.SubjectsRequest;
import uz.nuu.payload.SubjectsResponse;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class SubjectsConverter extends Converter<SubjectsResponse, Subjects> {
    public SubjectsConverter() {
        super(fromDto -> new Subjects(),
                fromEntity -> {
                    List<String> images = null;
                    if (fromEntity.getTeachersSubjects() != null) {
                        images = fromEntity.getTeachersSubjects().stream().map(teachers -> teachers.getImg()).collect(Collectors.toList());
                    }
                    return new SubjectsResponse(
                            fromEntity.getId(),
                            fromEntity.getName(),
                            fromEntity.getTitle(),
                            fromEntity.getContent(),
                            fromEntity.getImg(),
                            images
                    );

                }
        );
    }

    public Subjects convertFromRequest(SubjectsRequest request, Collection<Teachers> teachers) {
        return new Subjects(
                request.getName(),
                request.getTitle(),
                request.getContent(),
                request.getImg(),
                teachers
        );
    }

    public List<SubjectsResponse> convertFromEntitiesList(List<Subjects> studentsList) {
        return studentsList.stream()
                .map(fromEntity -> new SubjectsResponse(
                                fromEntity.getId(),
                                fromEntity.getName(),
                                fromEntity.getTitle(),
                                fromEntity.getContent(),
                                fromEntity.getImg(),
                                fromEntity.getTeachersSubjects() != null
                                        ? fromEntity.getTeachersSubjects().stream().map(teachers -> teachers.getImg()).collect(Collectors.toList()) : null
                        )
                )
                .collect(Collectors.toList());
    }
}
