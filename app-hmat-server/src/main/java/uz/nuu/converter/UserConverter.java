package uz.nuu.converter;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import uz.nuu.entity.Role;
import uz.nuu.entity.User;
import uz.nuu.payload.UserRequest;
import uz.nuu.payload.UserResponse;

import java.util.List;
import java.util.stream.Collectors;

public class UserConverter extends Converter<UserResponse, User> {

    public UserConverter() {
        super(fromDto -> new User(),
                fromEntity -> new UserResponse(
                        fromEntity.getId(),
                        fromEntity.getUsername(),
                        fromEntity.getEmail(),
                        fromEntity.getPassword(),
                        fromEntity.getFullName(),
                        fromEntity.getGender(),
                        fromEntity.getProvince(),
                        fromEntity.getDistrict(),
                        fromEntity.getRoles()
                )
        );
    }

    public User convertFromRequest(UserRequest request, List<Role> roles) {

        return new User(
                request.getUsername(),
                request.getEmail(),
                request.getGender(),
                request.getProvince(),
                request.getPassword(),
                roles
        );
    }

    public List<UserResponse> convertFromUserList(List<User> list) {
        return list
                .stream()
                .map(user -> new UserResponse(
                        user.getId(),
                        user.getUsername(),
                        user.getEmail(),
                        user.getPassword(),
                        user.getFullName(),
                        user.getGender(),
                        user.getProvince(),
                        user.getDistrict(),
                        user.getRoles()
                ))
                .collect(Collectors.toList());
    }

    public Page<UserResponse> convertFromPages(Page<User> page) {
        return new PageImpl<>(
                page.getContent()
                        .stream()
                        .map(user -> new UserResponse(
                                user.getId(),
                                user.getUsername(),
                                user.getEmail(),
                                user.getPassword(),
                                user.getFullName(),
                                user.getGender(),
                                user.getProvince(),
                                user.getDistrict(),
                                user.getRoles()
                        )).collect(Collectors.toList()), page.getPageable(), page.getTotalElements()
        );
    }
}
