package uz.nuu.converter;


import uz.nuu.entity.FileAvatar;
import uz.nuu.payload.FileAvatarUploadResponse;

public class FileAvatarConverter extends Converter<FileAvatarUploadResponse, FileAvatar> {

    public FileAvatarConverter() {
        super(fromFileAvatarResponse ->
                        new FileAvatar(
                                fromFileAvatarResponse.getId(),
                                fromFileAvatarResponse.getOriginalName(),
                                fromFileAvatarResponse.getGeneratedName()),
                fromFileAvatarEntity ->
                        new FileAvatarUploadResponse(
                                fromFileAvatarEntity.getId(),
                                fromFileAvatarEntity.getOriginalName(),
                                fromFileAvatarEntity.getGeneratedName()
                        ));
    }
}
