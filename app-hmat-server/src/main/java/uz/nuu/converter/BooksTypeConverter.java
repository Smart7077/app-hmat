package uz.nuu.converter;

import uz.nuu.entity.BooksType;
import uz.nuu.payload.BooksTypeRequest;
import uz.nuu.payload.BooksTypeResponse;

import java.util.List;
import java.util.stream.Collectors;

public class BooksTypeConverter extends Converter<BooksTypeResponse, BooksType> {
    public BooksTypeConverter() {
        super(fromDto -> new BooksType(),
                fromEntity -> new BooksTypeResponse(
                        fromEntity.getId(),
                        fromEntity.getName(),
                        fromEntity.getCreatedDate(),
                        fromEntity.getModifiedDate(),
                        fromEntity.getCreatedBy(),
                        fromEntity.getModifiedBy(),
                        fromEntity.getStatus()
                )
        );
    }

    public BooksType convertFromRequest(BooksTypeRequest request) {
        return new BooksType(
                request.getName(),
                request.getStatus()
        );
    }

    public List<BooksTypeResponse> convertFromEntitiesList(List<BooksType> booksTypeList) {

        return booksTypeList.stream()
                .map(booksType -> new BooksTypeResponse(
                                booksType.getId(),
                                booksType.getName(),
                                booksType.getCreatedDate(),
                                booksType.getModifiedDate(),
                                booksType.getCreatedBy(),
                                booksType.getModifiedBy(),
                                booksType.getStatus()
                        )
                )
                .collect(Collectors.toList());
    }
}
