package uz.nuu.converter;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import uz.nuu.entity.Teachers;
import uz.nuu.payload.TeachersRequest;
import uz.nuu.payload.TeachersResponse;

import java.util.List;
import java.util.stream.Collectors;

public class TeachersConverter extends Converter<TeachersResponse, Teachers> {
    public TeachersConverter() {
        super(fromDto -> new Teachers(),
                fromEntity -> new TeachersResponse(
                        fromEntity.getId(),
                        fromEntity.getFirstname(),
                        fromEntity.getLastName(),
                        fromEntity.getMiddleName(),
                        fromEntity.getPhoneNumber(),
                        fromEntity.getAddress(),
                        fromEntity.getImg(),
                        fromEntity.getEmail(),
                        fromEntity.getFacebook(),
                        fromEntity.getLinkedIn(),
                        fromEntity.getInstagram(),
                        fromEntity.getTelegram(),
                        fromEntity.getDegree(),
                        fromEntity.getContent(),
                        fromEntity.getCreatedDate(),
                        fromEntity.getModifiedDate(),
                        fromEntity.getCreatedBy(),
                        fromEntity.getModifiedBy(),
                        fromEntity.getStatus()
                )
        );
    }

    public Teachers convertFromRequest(TeachersRequest request) {
        return new Teachers(request.getFirstname(),
                request.getLastName(),
                request.getMiddleName(),
                request.getPhoneNumber(),
                request.getAddress(),
                request.getImg(),
                request.getEmail(),
                request.getFacebook(),
                request.getLinkedIn(),
                request.getInstagram(),
                request.getTelegram(),
                request.getDegree(),
                request.getContent(),
                request.getStatus());
    }

    public Page<TeachersResponse> convertFromEntities(Page<Teachers> teachers) {

        return new PageImpl<>(
                teachers.getContent()
                        .stream()
                        .map(item -> new TeachersResponse(
                                        item.getId(),
                                        item.getFirstname(),
                                        item.getLastName(),
                                        item.getMiddleName(),
                                        item.getPhoneNumber(),
                                        item.getAddress(),
                                        item.getImg(),
                                        item.getEmail(),
                                        item.getFacebook(),
                                        item.getLinkedIn(),
                                        item.getInstagram(),
                                        item.getTelegram(),
                                        item.getDegree(),
                                        item.getContent(),
                                        item.getCreatedDate(),
                                        item.getModifiedDate(),
                                        item.getCreatedBy(),
                                        item.getModifiedBy(),
                                        item.getStatus()
                                )
                        )
                        .collect(Collectors.toList()), teachers.getPageable(), teachers.getTotalElements()
        );
    }

    public List<TeachersResponse> convertFromEntitiesList(List<Teachers> teachersList) {

        return teachersList.stream()
                .map(item -> new TeachersResponse(
                        item.getId(),
                        item.getFirstname(),
                        item.getLastName(),
                        item.getMiddleName(),
                        item.getPhoneNumber(),
                        item.getAddress(),
                        item.getImg(),
                        item.getEmail(),
                        item.getFacebook(),
                        item.getLinkedIn(),
                        item.getInstagram(),
                        item.getTelegram(),
                        item.getDegree(),
                        item.getContent(),
                        item.getCreatedDate(),
                        item.getModifiedDate(),
                        item.getCreatedBy(),
                        item.getModifiedBy(),
                        item.getStatus()
                ))
                .collect(Collectors.toList());
    }
}
