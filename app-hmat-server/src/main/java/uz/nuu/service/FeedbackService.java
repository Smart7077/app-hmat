package uz.nuu.service;

import uz.nuu.entity.Feedback;
import uz.nuu.exception.ApiException;
import uz.nuu.exception.DataAlreadyExistsException;
import uz.nuu.exception.DataNotFoundException;

import java.util.List;

public interface FeedbackService {
    Feedback create(Feedback request);

    Feedback getById(Long id) throws DataNotFoundException;

    boolean delete(Long id) throws DataNotFoundException;

    List<Feedback> list() throws ApiException;
}
