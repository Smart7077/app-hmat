package uz.nuu.service;

import uz.nuu.entity.Students;
import uz.nuu.exception.ApiException;
import uz.nuu.exception.DataAlreadyExistsException;
import uz.nuu.exception.DataNotFoundException;

import java.util.List;
import java.util.UUID;

public interface StudentService {

    Students create(Students request) throws DataAlreadyExistsException;

    Students getById(UUID id) throws DataNotFoundException;

    Students update(UUID id, Students request) throws DataNotFoundException, DataAlreadyExistsException;

    boolean delete(UUID id) throws DataNotFoundException;

    List<Students> list() throws ApiException;
}
