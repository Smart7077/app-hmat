package uz.nuu.service;

import uz.nuu.entity.Category;
import uz.nuu.exception.ApiException;
import uz.nuu.exception.DataNotFoundException;
import uz.nuu.payload.CategoryRequest;

import java.util.List;
import java.util.UUID;

public interface CategoryService {
    Category create(Category request);

    Category getById(UUID id) throws DataNotFoundException;

    boolean delete(UUID id) throws DataNotFoundException;

    List<Category> list() throws ApiException;

    Category update(CategoryRequest request, UUID id);
}
