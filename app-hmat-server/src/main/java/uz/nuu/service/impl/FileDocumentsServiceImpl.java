package uz.nuu.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import uz.nuu.exception.DataNotFoundException;
import uz.nuu.repository.FileDocumentsRepository;
import uz.nuu.entity.FileDocuments;
import uz.nuu.exception.FileUploadException;
import uz.nuu.exception.InvalidDataException;
import uz.nuu.service.FileDocumentsService;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class FileDocumentsServiceImpl implements FileDocumentsService {

    private static final Logger logger = LogManager.getLogger("ServiceLogger");

    private final FileDocumentsRepository fileDocumentsRepository;

    private FileWriterReaderImpl fileWriterReader;

    public FileDocumentsServiceImpl(FileDocumentsRepository fileDocumentsRepository) {
        this.fileDocumentsRepository = fileDocumentsRepository;
        this.fileWriterReader = new FileWriterReaderImpl();
    }

    @Override
    @Transactional
//    @PreAuthorize("@SecurityPermission.hasPermission('PERSONAL_WRITE')")
    public List<FileDocuments> documentsUpload(List<MultipartFile> multipartFiles, String documentType) throws FileUploadException, InvalidDataException {


        List<FileDocuments> fileDocumentsEntities = new ArrayList<>();

        for (MultipartFile multipartFile : multipartFiles) {

            String contentType = null;

            if (multipartFile.getContentType().toLowerCase().equals("application/pdf")) {
                contentType = ".pdf";
            }

            if (multipartFile.getContentType().toLowerCase().equals("application/msword")) {
                contentType = ".doc";
            }

            if (multipartFile.getContentType().toLowerCase().equals("application/vnd.openxmlformats-officedocument.wordprocessingml.document")) {
                contentType = ".docx";
            }

            if (contentType == null) {
                throw new InvalidDataException("Hujjat pdf,doc formatda emas");
            }

            FileDocuments fileDocumentsEntity = new FileDocuments();

            fileDocumentsEntity.setDocumentType(documentType);
            fileDocumentsEntity.setOriginalName(multipartFile.getOriginalFilename());
            fileDocumentsEntity.setGeneratedName(fileWriterReader.generateFileName() + contentType);
            fileDocumentsEntity.setCreatedDate(new Date(System.currentTimeMillis()));
            logger.info(fileDocumentsEntity.toString());

            fileWriterReader.documentWrite(
                    multipartFile,
                    fileDocumentsEntity.getDocumentType(),
                    fileDocumentsEntity.getGeneratedName(),
                    fileDocumentsEntity.getCreatedDate());

            fileDocumentsEntities.add(fileDocumentsRepository.save(fileDocumentsEntity));

        }

        return fileDocumentsEntities;
    }

    @Override
    @Transactional
    public Resource getDocumentAsResourceById(UUID id) throws DataNotFoundException, MalformedURLException {

        Resource resource = null;

        FileDocuments fileDocumentsEntity = fileDocumentsRepository.findById(id)
                .orElseThrow(() -> new DataNotFoundException("Data Not Found"));

        resource = fileWriterReader.getDocumentsAsResource(
                fileDocumentsEntity.getGeneratedName(),
                fileDocumentsEntity.getDocumentType(),
                fileDocumentsEntity.getCreatedDate());

        return resource;
    }


    @Override
    @Transactional
    public Resource getDocumentAsResourceByGeneratedName(String generatedName) throws DataNotFoundException, MalformedURLException {

        Resource resource = null;

        FileDocuments fileDocumentsEntity = fileDocumentsRepository.findByGeneratedName(generatedName)
                .orElseThrow(() -> new DataNotFoundException("Data Not Found"));

        resource = fileWriterReader.getDocumentsAsResource(
                fileDocumentsEntity.getGeneratedName(),
                fileDocumentsEntity.getDocumentType(),
                fileDocumentsEntity.getCreatedDate());

        return resource;
    }

    @Override
    public FileDocuments getByGeneratedName(String docGeneratedName) {

        return fileDocumentsRepository.findByGeneratedName(docGeneratedName)
                .orElseThrow(() -> new DataNotFoundException("File Info Not Found"));
    }

    @Override
    public UUID saveFile(String generationName, String orginalName, String documentType) {
        FileDocuments fileDocumentsEntity = new FileDocuments();

        fileDocumentsEntity.setDocumentType(documentType);
        fileDocumentsEntity.setOriginalName(generationName);
        fileDocumentsEntity.setGeneratedName(generationName);
        fileDocumentsEntity.setCreatedDate(new Date(System.currentTimeMillis()));

        logger.info(fileDocumentsEntity.toString());

        FileDocuments entity = fileDocumentsRepository.save(fileDocumentsEntity);
        return entity.getId();
    }
}
