package uz.nuu.service.impl;

import org.springframework.stereotype.Service;
import uz.nuu.entity.Teachers;
import uz.nuu.exception.ApiException;
import uz.nuu.exception.DataAlreadyExistsException;
import uz.nuu.exception.DataNotFoundException;
import uz.nuu.repository.TeachersRepository;
import uz.nuu.service.TeachersService;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class TeachersImpl implements TeachersService {

    private final TeachersRepository teachersRepository;

    public TeachersImpl(TeachersRepository teachersRepository) {
        this.teachersRepository = teachersRepository;
    }

    @Override
    public Teachers create(Teachers request) throws DataAlreadyExistsException {
        request.setCreatedDate(new Date(System.currentTimeMillis()));
        request.setModifiedDate(new Date(System.currentTimeMillis()));
        return teachersRepository.save(request);
    }

    @Override
    public Teachers getById(UUID id) throws DataNotFoundException {
        return teachersRepository.findById(id).orElseThrow(() -> new DataNotFoundException("Data not found"));
    }

    @Override
    public Teachers update(UUID id, Teachers request) throws DataNotFoundException, DataAlreadyExistsException {
        Teachers teachers = teachersRepository.findById(id).orElseThrow(() -> new DataNotFoundException("Data not found!!!"));
        teachers.setFirstname(request.getFirstname());
        teachers.setLastName(request.getLastName());
        teachers.setMiddleName(request.getMiddleName());
        teachers.setPhoneNumber(request.getPhoneNumber());
        teachers.setAddress(request.getAddress());
        teachers.setImg(request.getImg());
        teachers.setEmail(request.getEmail());
        teachers.setFacebook(request.getFacebook());
        teachers.setLinkedIn(request.getLinkedIn());
        teachers.setInstagram(request.getInstagram());
        teachers.setTelegram(request.getTelegram());
        teachers.setDegree(request.getDegree());
        teachers.setContent(request.getContent());
        teachers.setCreatedDate(request.getCreatedDate());
        teachers.setModifiedDate(new Date(System.currentTimeMillis()));
        teachers.setCreatedBy(request.getCreatedBy());
        teachers.setModifiedBy(request.getModifiedBy());
        teachers.setStatus(request.getStatus());
        return teachersRepository.save(teachers);
    }

    @Override
    public boolean delete(UUID id) throws DataNotFoundException {
        if (getById(id) != null) {
            teachersRepository.deleteById(id);
            return true;
        } else {
            throw new DataNotFoundException("Data not found");
        }
    }

    @Override
    public List<Teachers> list() throws ApiException {
        return teachersRepository.findAll();
    }

    @Override
    public List<Teachers> findAllByIdIn(List<UUID> idList) {
        return teachersRepository.findAllByIdIn(idList);
    }
}
