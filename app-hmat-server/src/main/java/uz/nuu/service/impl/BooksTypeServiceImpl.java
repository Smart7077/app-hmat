package uz.nuu.service.impl;

import org.springframework.stereotype.Service;
import uz.nuu.entity.BooksType;
import uz.nuu.exception.ApiException;
import uz.nuu.exception.DataAlreadyExistsException;
import uz.nuu.exception.DataNotFoundException;
import uz.nuu.repository.BooksTypeRepository;
import uz.nuu.service.BooksTypeService;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class BooksTypeServiceImpl implements BooksTypeService {
    private final BooksTypeRepository booksTypeRepository;

    public BooksTypeServiceImpl(BooksTypeRepository booksTypeRepository) {
        this.booksTypeRepository = booksTypeRepository;
    }

    @Override
    public BooksType create(BooksType request) throws DataAlreadyExistsException {
        return booksTypeRepository.save(request);
    }

    @Override
    public BooksType getById(UUID id) throws DataNotFoundException {
        return booksTypeRepository.findById(id).orElseThrow(() -> new DataNotFoundException("Data not found exception"));
    }

    @Override
    public BooksType update(UUID id, BooksType request) throws DataNotFoundException, DataAlreadyExistsException {
        BooksType findBooksType = getById(id);
        findBooksType.setName(request.getName());
        findBooksType.setStatus(request.getStatus());
        findBooksType.setModifiedDate(new Date(System.currentTimeMillis()));
        return booksTypeRepository.save(findBooksType);
    }

    @Override
    public boolean delete(UUID id) throws DataNotFoundException {
        if (getById(id) != null) {
            booksTypeRepository.deleteById(id);
            return true;
        } else {
            throw new DataNotFoundException("Data not found");
        }
    }

    @Override
    public List<BooksType> list() throws ApiException {
        return booksTypeRepository.findAll();
    }
}
