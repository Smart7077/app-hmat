package uz.nuu.service.impl;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.nuu.entity.Permission;
import uz.nuu.exception.DataAlreadyExistsException;
import uz.nuu.exception.DataNotFoundException;
import uz.nuu.repository.PermissionRepository;
import uz.nuu.service.PermissionService;

import java.util.List;

@Service
public class PermissionServiceImpl implements PermissionService {

    private final PermissionRepository permissionRepository;

    public PermissionServiceImpl(PermissionRepository permissionRepository) {
        this.permissionRepository = permissionRepository;
    }

    @Override
    @Transactional
    public Permission create(Permission request) throws DataAlreadyExistsException {
        if (permissionRepository.findByTitle(request.getTitle()) == null) {
            return permissionRepository.save(request);
        } else {
            throw new DataAlreadyExistsException("This data already exists");
        }

    }

    @Override
    @Transactional
    public Permission getById(Long id) throws DataNotFoundException {
        return permissionRepository
                .findById(id)
                .orElseThrow(() -> new DataNotFoundException("Data not found id = " + id));
    }

    @Override
    public Page<Permission> list(Pageable pageable) {
        return permissionRepository.findAll(pageable);
    }

    @Override
    public List<Permission> list() {
        return permissionRepository.findAll();
    }

    @Override
    public List<Permission> findByIds(List<Long> ids) {
        return permissionRepository.findAllById(ids);
    }

    @Override
    public Permission getByTitle(String title) {
        return permissionRepository.findByTitle(title);
    }

    @Override
    public Permission update(Long id, Permission request) throws DataNotFoundException, DataAlreadyExistsException {
        try {
            Permission permission = getById(id);
            if (getByTitle(request.getTitle()) != null) {
                throw new DataAlreadyExistsException("This data already exists");
            }
            permission.setName(request.getName());
            permission.setTitle(request.getTitle());
            return permissionRepository.save(permission);
        } catch (ConstraintViolationException e) {
            throw new DataAlreadyExistsException("This data already exists");
        }
    }

    @Override
    public boolean delete(Long id) throws DataNotFoundException {
        if (getById(id) != null) {
            permissionRepository.deleteById(id);
            return true;
        } else {
            throw new DataNotFoundException("Data not found id = " + id);
        }
    }


}
