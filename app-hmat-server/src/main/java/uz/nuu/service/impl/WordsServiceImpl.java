package uz.nuu.service.impl;

import org.springframework.stereotype.Service;
import uz.nuu.entity.Words;
import uz.nuu.exception.ApiException;
import uz.nuu.exception.DataNotFoundException;
import uz.nuu.payload.WordsRequest;
import uz.nuu.repository.WordsRepository;
import uz.nuu.service.WordsService;

import java.util.List;

@Service
public class WordsServiceImpl implements WordsService {

    private WordsRepository repository;

    public WordsServiceImpl(WordsRepository repository) {
        this.repository = repository;
    }

    @Override
    public Words create(Words request) {
        return repository.save(request);
    }

    @Override
    public Words getById(Long id) throws DataNotFoundException {
        return repository.findById(id).orElseThrow(() -> new DataNotFoundException("Data not found"));
    }

    @Override
    public boolean delete(Long id) throws DataNotFoundException {
        if (getById(id) != null) {
            repository.deleteById(id);
            return true;
        } else {
            throw new DataNotFoundException("Data not found");
        }
    }

    @Override
    public List<Words> list() throws ApiException {
        return repository.findAll();
    }

    @Override
    public Words update(WordsRequest request, Long id) {
        Words words = getById(id);
        words.setName(request.getName());
        return repository.save(words);
    }
}
