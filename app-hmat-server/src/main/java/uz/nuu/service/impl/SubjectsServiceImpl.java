package uz.nuu.service.impl;

import org.springframework.stereotype.Service;
import uz.nuu.entity.Subjects;
import uz.nuu.exception.ApiException;
import uz.nuu.exception.DataNotFoundException;
import uz.nuu.repository.SubjectsRepository;
import uz.nuu.service.SubjectsService;

import java.util.List;
import java.util.UUID;

@Service
public class SubjectsServiceImpl implements SubjectsService {
    private final SubjectsRepository subjectsRepository;

    public SubjectsServiceImpl(SubjectsRepository subjectsRepository) {
        this.subjectsRepository = subjectsRepository;
    }

    @Override
    public Subjects create(Subjects request) {
        return subjectsRepository.save(request);
    }

    @Override
    public Subjects getById(UUID id) throws DataNotFoundException {
        return subjectsRepository.findById(id).orElseThrow(() -> new DataNotFoundException("Data not found"));
    }

    @Override
    public boolean delete(UUID id) throws DataNotFoundException {
        if (getById(id) != null) {
            subjectsRepository.deleteById(id);
            return true;
        }
        return false;
    }

    @Override
    public List<Subjects> list() throws ApiException {
        List<Subjects> list = subjectsRepository.findAll();
        return list;
    }

    @Override
    public Subjects update(Subjects request, UUID id) {
        Subjects subjects = getById(id);
        subjects.setContent(request.getContent());
        subjects.setImg(request.getImg());
        subjects.setName(request.getName());
        subjects.setTeachersSubjects(request.getTeachersSubjects());
        subjects.setTitle(request.getTitle());
        return subjectsRepository.save(subjects);
    }
}
