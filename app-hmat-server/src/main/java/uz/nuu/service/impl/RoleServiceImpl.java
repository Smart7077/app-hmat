package uz.nuu.service.impl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import uz.nuu.entity.Role;
import uz.nuu.exception.ApiException;
import uz.nuu.exception.DataAlreadyExistsException;
import uz.nuu.exception.DataNotFoundException;
import uz.nuu.repository.RoleRepository;
import uz.nuu.service.RoleService;

import java.util.List;
import java.util.Optional;

@Service
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;

    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public Role create(Role request) throws ApiException {
        if (getBySystemName(request.getSystemName()) != null) {
            throw new DataAlreadyExistsException(" SystemName = " + request.getSystemName() + " data already exception");
        }
        return roleRepository.save(request);
    }

    @Override
    public Role update(Long id, Role request) throws ApiException {
        Optional<Role> findRole = roleRepository.findById(id);
        if (findRole.isEmpty()) {
            throw new DataNotFoundException("Role id = " + id + " not found");
        }
        if (getBySystemName(request.getSystemName()) != null) {
            throw new DataAlreadyExistsException(" SystemName = " + request.getSystemName() + " data already exception");
        }
        Role role = findRole.get();
        role.setActive(request.getActive());
        role.setDescription(request.getDescription());
        role.setName(request.getName());
        role.setPermissions(request.getPermissions());
        role.setSystemName(request.getSystemName());
        return roleRepository.save(role);
    }

    @Override
    public Role getById(Long id) throws ApiException {

        return roleRepository.findById(id).orElseThrow(() -> new DataNotFoundException("id = " + id + " role not found"));
    }

    @Override
    public Page<Role> list(Pageable pageable) throws ApiException {
        return roleRepository.findAll(pageable);
    }

    @Override
    public boolean delete(Long id) throws DataNotFoundException {
        if (roleRepository.existsById(id) == false) {
            throw new DataNotFoundException("id = " + id + " data not found");
        }
        roleRepository.deleteById(id);
        return true;
    }

    @Override
    public List<Role> list() throws DataNotFoundException {
        return roleRepository.findAll();
    }

    @Override
    public Role getBySystemName(String name) {
        return roleRepository.getBySystemName(name);
    }

    @Override
    public List<Role> findByIds(List<Long> ids) {
        return roleRepository.findAllByIdIn(ids);
    }
}
