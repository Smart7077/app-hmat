package uz.nuu.service.impl;

import org.springframework.stereotype.Service;
import uz.nuu.entity.Prizes;
import uz.nuu.entity.Students;
import uz.nuu.exception.ApiException;
import uz.nuu.exception.DataAlreadyExistsException;
import uz.nuu.exception.DataNotFoundException;
import uz.nuu.payload.PrizesRequest;
import uz.nuu.repository.PrizeRepository;
import uz.nuu.repository.StudentsRepository;
import uz.nuu.service.PrizeService;

import java.util.List;
import java.util.UUID;
@Service
public class PrizeServiceImpl implements PrizeService {

    private final PrizeRepository repository;

    public PrizeServiceImpl(PrizeRepository repository) {
        this.repository = repository;
    }

    @Override
    public Prizes create(Prizes request) throws DataAlreadyExistsException {
        return null;
    }

    @Override
    public Prizes getById(UUID id) throws DataNotFoundException {
        return null;
    }

    @Override
    public Prizes update(UUID id, PrizesRequest request, Students students) throws DataNotFoundException, DataAlreadyExistsException {
        return null;
    }

    @Override
    public boolean delete(UUID id) throws DataNotFoundException {
        return false;
    }

    @Override
    public List<Prizes> list() throws ApiException {
        return null;
    }
}
