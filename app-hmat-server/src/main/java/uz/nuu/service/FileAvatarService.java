package uz.nuu.service;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;
import uz.nuu.entity.FileAvatar;
import uz.nuu.exception.DataNotFoundException;
import uz.nuu.exception.FileUploadException;
import uz.nuu.exception.InvalidDataException;

import java.util.UUID;

public interface FileAvatarService {

    FileAvatar avatarUpload(MultipartFile multipartFile) throws FileUploadException, InvalidDataException;

    Resource getAvatarImgAsResourceByID(UUID id) throws DataNotFoundException;

    Resource getAvatarImgAsResourceByGeneratedName(String fileName) throws DataNotFoundException;
}
