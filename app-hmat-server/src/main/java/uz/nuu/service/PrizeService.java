package uz.nuu.service;

import uz.nuu.entity.Prizes;
import uz.nuu.entity.Students;
import uz.nuu.exception.ApiException;
import uz.nuu.exception.DataAlreadyExistsException;
import uz.nuu.exception.DataNotFoundException;
import uz.nuu.payload.PrizesRequest;

import java.util.List;
import java.util.UUID;

public interface PrizeService {
    Prizes create(Prizes request) throws DataAlreadyExistsException;

    Prizes getById(UUID id) throws DataNotFoundException;

    Prizes update(UUID id, PrizesRequest request, Students students) throws DataNotFoundException, DataAlreadyExistsException;

    boolean delete(UUID id) throws DataNotFoundException;

    List<Prizes> list() throws ApiException;
}
