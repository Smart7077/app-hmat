package uz.nuu.service;

import uz.nuu.entity.Books;
import uz.nuu.entity.BooksType;
import uz.nuu.exception.ApiException;
import uz.nuu.exception.DataAlreadyExistsException;
import uz.nuu.exception.DataNotFoundException;
import uz.nuu.payload.BooksRequest;

import java.util.List;
import java.util.UUID;

public interface BooksService {
    Books create(Books request) throws DataAlreadyExistsException;

    Books getById(UUID id) throws DataNotFoundException;

    Books update(UUID id, BooksRequest request, BooksType booksType) throws DataNotFoundException, DataAlreadyExistsException;

    boolean delete(UUID id) throws DataNotFoundException;

    List<Books> list() throws ApiException;
}
