package uz.nuu.service;

import uz.nuu.entity.Articles;
import uz.nuu.exception.ApiException;
import uz.nuu.exception.DataAlreadyExistsException;
import uz.nuu.exception.DataNotFoundException;
import uz.nuu.payload.ArticlesRequest;

import java.util.List;
import java.util.UUID;

public interface ArticlesService {
    Articles create(Articles request) throws DataAlreadyExistsException;

    Articles getById(UUID id) throws DataNotFoundException;

    Articles update(UUID id, ArticlesRequest request) throws DataNotFoundException, DataAlreadyExistsException;

    boolean delete(UUID id) throws DataNotFoundException;

    List<Articles> list() throws ApiException;
}
