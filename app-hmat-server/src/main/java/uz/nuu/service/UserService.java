package uz.nuu.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import uz.nuu.entity.Role;
import uz.nuu.entity.User;
import uz.nuu.exception.ApiException;
import uz.nuu.exception.DataNotFoundException;

import java.util.List;
import java.util.Optional;

public interface UserService {
    User create(User request) throws ApiException;

    User findById(Long id) throws ApiException;

    Page<User> list(Pageable pageable) throws ApiException;

    User update(Long id, User request) throws ApiException;

    boolean delete(Long id) throws ApiException;

    List<User> list() throws ApiException;

    Boolean existsByEmail(String email);

    Boolean existsByUsername(String username) ;

    List<Role> findRolesByUsername(String username) throws DataNotFoundException;

    User findByName(String name) throws DataNotFoundException;
}
