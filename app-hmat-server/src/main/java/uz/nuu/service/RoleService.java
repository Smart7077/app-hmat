package uz.nuu.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import uz.nuu.entity.Permission;
import uz.nuu.entity.Role;
import uz.nuu.exception.ApiException;
import uz.nuu.exception.DataAlreadyExistsException;
import uz.nuu.exception.DataNotFoundException;

import java.util.List;

public interface RoleService {
    Role create(Role request) throws ApiException;

    Role getById(Long id) throws ApiException;

    Page<Role> list(Pageable pageable) throws ApiException;

    Role update(Long id, Role request) throws DataNotFoundException, DataAlreadyExistsException;

    boolean delete(Long id) throws ApiException;

    List<Role> list() throws DataNotFoundException;

    Role getBySystemName(String name);

    List<Role> findByIds(List<Long> ids);
}
