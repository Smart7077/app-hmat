package uz.nuu.service;

import uz.nuu.entity.Words;
import uz.nuu.exception.ApiException;
import uz.nuu.exception.DataNotFoundException;
import uz.nuu.payload.WordsRequest;

import java.util.List;

public interface WordsService {
    Words create(Words request);

    Words getById(Long id) throws DataNotFoundException;

    boolean delete(Long id) throws DataNotFoundException;

    List<Words> list() throws ApiException;

    Words update(WordsRequest request, Long id);
}
