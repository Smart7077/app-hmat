package uz.nuu.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import uz.nuu.entity.News;
import uz.nuu.exception.ApiException;
import uz.nuu.exception.DataAlreadyExistsException;
import uz.nuu.exception.DataNotFoundException;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public interface NewsService {
    News create(News request) throws DataAlreadyExistsException;

    News update(UUID id, News request) throws DataNotFoundException, DataAlreadyExistsException;

    Page<News> list(Pageable pageable) throws ApiException;

    News getById(UUID id) throws DataNotFoundException;

    boolean delete(UUID id) throws DataNotFoundException;

    List<News> list() throws ApiException;

    News getByTitle(HashMap<String, String> title);

    Page<News> getByCategory(Pageable pageable,UUID uuid);
}
