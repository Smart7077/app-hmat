package uz.nuu.service;

import uz.nuu.entity.ProductionProcess;
import uz.nuu.exception.ApiException;
import uz.nuu.exception.DataAlreadyExistsException;
import uz.nuu.exception.DataNotFoundException;

import java.util.List;
import java.util.UUID;

public interface ProductionProductService {
    ProductionProcess create(ProductionProcess request) throws DataAlreadyExistsException;

    ProductionProcess getById(UUID id) throws DataNotFoundException;

    ProductionProcess update(UUID id, ProductionProcess request) throws DataNotFoundException, DataAlreadyExistsException;

    boolean delete(UUID id) throws DataNotFoundException;

    List<ProductionProcess> list() throws ApiException;
}
