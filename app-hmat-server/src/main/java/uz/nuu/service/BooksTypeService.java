package uz.nuu.service;

import uz.nuu.entity.BooksType;
import uz.nuu.exception.ApiException;
import uz.nuu.exception.DataAlreadyExistsException;
import uz.nuu.exception.DataNotFoundException;
import uz.nuu.payload.BooksTypeRequest;

import java.util.List;
import java.util.UUID;

public interface BooksTypeService {
    BooksType create(BooksType request) throws DataAlreadyExistsException;

    BooksType getById(UUID id) throws DataNotFoundException;

    BooksType update(UUID id, BooksType request) throws DataNotFoundException, DataAlreadyExistsException;

    boolean delete(UUID id) throws DataNotFoundException;

    List<BooksType> list() throws ApiException;

}
