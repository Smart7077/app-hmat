package uz.nuu.exception;

public class FileUploadException extends ApiException {
    private static final long serialVersionUID = 1L;

    public FileUploadException() {
    }

    public FileUploadException(String message) {
        super(message);
    }
}

