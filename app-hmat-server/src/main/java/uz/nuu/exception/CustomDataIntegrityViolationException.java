package uz.nuu.exception;

public class CustomDataIntegrityViolationException extends ApiException{

    public CustomDataIntegrityViolationException() {
    }

    public CustomDataIntegrityViolationException(String message) {
        super(message);
    }
}
