package uz.nuu.dbInsert;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import uz.nuu.entity.Permission;
import uz.nuu.entity.Role;
import uz.nuu.entity.User;
import uz.nuu.repository.PermissionRepository;
import uz.nuu.repository.RoleRepository;
import uz.nuu.repository.UserRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class DbInsert implements CommandLineRunner {

    private PermissionRepository permissionRepository;
    private RoleRepository roleRepository;
    private UserRepository userRepository;

    public DbInsert(PermissionRepository permissionRepository, RoleRepository roleRepository, UserRepository userRepository) {
        this.permissionRepository = permissionRepository;
        this.roleRepository = roleRepository;
        this.userRepository = userRepository;
    }


    @Override
    public void run(String... args) throws Exception {
        Permission reader = new Permission(1L, "READ_PERMISSION", "READ PERMISSION");
        Permission writer = new Permission(2L, "WRITE_PERMISSION", "WRITE PERMISSION");
        Permission updater = new Permission(3L, "UPDATE_PERMISSION", "UPDATE PERMISSION");
        Permission deleter = new Permission(4L, "DELETE_PERMISSION", "DELETE PERMISSION");
        List<Permission> permissionEntityList = new ArrayList<>(Arrays.asList(reader, writer, updater, deleter));
        permissionRepository.saveAll(permissionEntityList);

        Role admin = new Role("admin", "admin", "Tizimdagi barcha huquqlarga ega bo'lgan role", true);
        admin.setPermissions(Arrays.asList(reader, writer, updater));

        roleRepository.saveAll(Arrays.asList(admin));

        List<User> userList = new ArrayList<>();
        User user9601 = new User(1L, "user9601", "$2a$10$AavQPzo6WTpJafATc4piaO8C8At8cYqfhE7hQNVd3Hv51aM3jRVa2");
        user9601.setRoles(new ArrayList<>(Arrays.asList(admin)));

        userRepository.saveAll(new ArrayList<>(Arrays.asList(user9601)));
    }
}
